#!/usr/bin/env bash
azure_upload() {
    [[ "${1}" == "WIPE" ]] && FileCount="$(( $HDDCOUNT + 1 ))" || FileCount="1"
    wait_for_network && grep -q -c -m "1" "Total files transferred: ${FileCount}" <(azcopy --quiet --recursive --source "${PCDDIR}testlogs" --destination "${AZURE}" --include "${AssetID}-${1}*") >> "${PCDDIR}Logging/tracelog" 2>&1
    [[ "$?" != "0" ]] && { rm -r /root/Microsoft/Azure/AzCopy/*; azure_upload "${1}"; } || syncronize_variables ${1}Upload="${TRUE}"
    return "0"
}
wait_for_network() {
    declare -i Count="0"
    until [[ "$(ip route | grep default)" || "${Count}" == "20" ]]; do
        [[ "${Count}" == "5" ]] && dhclient -x 2>> ${PCDDIR}Logging/tracelog && dhclient
        [[ "${Count}" == "10" ]] && systemctl restart wicd
        sleep "2"
        Count+="1"
    done
    [[ "${Count}" == "20" && ! "$(ip route | grep default)" ]] && read -n "1" -p "Network connection lost, please check the cable and press any key to retry." && wait_for_network
    return "0"
}
syncronize_variables() {
    while [[ "$#" -gt 0 ]]; do
        [[ "${1}" != "${1%%=*}" ]] && declare -g -x "${1}"
        if [[ "${1%%=*}" != "Note" ]]; then
            declare -p ${1%%=*} > "${PCDDIR}Variables/${1%%=*}" 2>> "${PCDDIR}Logging/tracelog"
            sed -i "s/declare/declare -g/" "${PCDDIR}Variables/${1%%=*}" 2>> "${PCDDIR}Logging/tracelog"
        else
            [[ -e "${PCDDIR}Variables/Notes" ]] && source "${PCDDIR}Variables/Notes"
            declare -g -x Notes="$(echo -e "${Note} ${Notes}" | awk '{$1=$1;print}')"
            echo "declare -g -x Notes=\"${Notes}\"" > "${PCDDIR}Variables/Notes" 2>> "${PCDDIR}Logging/tracelog"
        fi
        shift
    done
    return "0"
}
display_status() {
    echo -e "Asset ID: ${AssetID:-Undetermined}      Technician ID: ${TechID:-Undetermined}
Manufacturer: $MANUFACTURER      Model: $MODEL      CPU: $CPU

Cosmetic Grade: ${CosmeticGrade:-Undetermined}      Functional Grade: ${FunctionalGrade:-Undetermined}      Inventory Status: ${AssetStatus:-Undetermined}
Auxiliary Field 1: ${AuxField1:-Empty}      Auxiliary Field 2: ${AuxField2:-Empty}      Notes: ${Notes:-Empty}"
    [[ "$HEADLESS" != "True" ]] && echo -e -n "Keyboard Language: ${KeyboardLanguage:-Undetermined}      "
    [[ "$HEADLESS" != "True" ]] && echo -e -n "Screen Size: ${ScreenSize:-Undetermined}\"       "
    [[ "$HEADLESS" != "True" ]] && echo -e -n "AC-Adapter: ${ACAdapter:-Undetermined}       "
    [[ "$SERVER" != "True" ]] && echo -e -n "OS Type: ${OSType:-"Not Installed"}      "
    [[ "$HEADLESS" != "True" ]] && echo -e -n "Webcam: ${Webcam:-Undetermined}       "
    [[ "$SERVER" == "True" ]] && echo -e -n "Hard Drive Bay Count: ${HDDBayCount:-Undetermined}       "
    echo -e "\n"
    echo -e -n "HDD Health: ${SmartTest:-${NOTRUN}}      Battery Test: ${BatteryTest:-${NOTRUN}}      Erasure: ${Erasure:-${NOTRUN}}      Diagnostics: ${Diagnostic:-${NOTRUN}}
System Information Uploaded: ${SYSINFOUpload:-${FALSE}}   "
    [[ "$Erasure" ]] && echo -e -n "Erasure Log Uploaded: ${WIPEUpload:-${FALSE}}   "
    [[ "$Diagnostic" ]] && echo -e -n "Diagnostic Log Uploaded: ${DIAGUpload:-${FALSE}}   "
    echo -e -n "GR Log Uploaded: ${GRVariablesUpload:-${FALSE}}"
    [[ "$Diagnostic" == "${FAIL}" ]] && display_failed_components
    [[ "$InvalidModel" == "True" ]] && echo -e "\n\n\e[5m\e[38;2;255;0;5mThis system contains invalid model information.\nPlease update the model manually in the GRID after integration has completed.\e[0m"
    return
}
generate_grvariables_file() {
    clear
    echo -e "The following values will be uploaded to the GRID.
If any of these values are missing or incorrect, please make any necessary updates manually.\n"
    display_status
    echo -e "TechnicianUserName: $TechID
AssetStatus: $AssetStatusCode
CosmeticGrade: $CosmeticGradeCode
FunctionalGrade: $FunctionalGradeCode" > "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$AuxField1" ]] && echo -e "AuxField1: $AuxField1" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$AuxField2" ]] && echo -e "AuxField2: $AuxField2" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$Notes" ]] && echo -e "Notes: $Notes" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$Notes" ]] && echo -e "Comments: $Notes" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$Webcam" ]] && echo -e "Webcam: $Webcam" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$KeyboardLanguage" ]] && echo -e "KeyboardLanguage: $KeyboardLanguage" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$ScreenSize" ]] && echo -e "ScreenSize: $ScreenSize" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$ACAdapter" ]] && echo -e "ACAdapter: $ACAdapter" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$OSType" ]] && echo -e "OSType: $OSType" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$HDDBayCount" ]] && echo -e "HDDBayCount: $HDDBayCount" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    syncronize_variables GRVariables="True"
    [[ "${AssetID}" ]] && azure_upload GRVariables
    [[ "$?" == "0" ]] && clear && echo -e "The following values will be uploaded to the GRID.\nIf any of these values are missing or incorrect, please make any necessary updates manually.\n" && display_status
    echo -e "\n"
    read -n "1" -p "Press any key to continue."
    return
}
upload_logs() {
    update_logs
    [[ -e "${PCDDIR}testlogs/${AssetID}-SYSINFO.yaml" && "$SYSINFOUpload" != "${TRUE}" ]] && { echo -e "TechnicianUserName: $TechID" >> "${PCDDIR}testlogs/${AssetID}-SYSINFO.yaml"; azure_upload SYSINFO; }
    [[ -e "${PCDDIR}testlogs/${AssetID}-SMART.csv" && "$SMARTUpload" != "${TRUE}" ]] && { echo -e "TechnicianUserName: $TechID" >> "${PCDDIR}testlogs/${AssetID}-SMART.csv"; azure_upload SMART; }
    if [[ -e "/tmp/erase_tmp_files/${AssetID}-WIPE-DISK1.html" && "$WIPEUpload" != "${TRUE}" ]]; then
        echo -e "TechnicianUserName: $TechID\nErasureStandard: NIST Clear\nVerificationPercentage: 100" >> "${PCDDIR}testlogs/${AssetID}-WIPE.csv"
        ${PCDDIR}drive_erase_workflow.pl --autogenreports >> "${PCDDIR}Logging/tracelog" 2>&1
        [[ "$?" != 0 ]] && read -n "1" -p "There was an error generating the erasure certificate. Please inform a team lead."
        azure_upload WIPE
    fi
    [[ -e "${PCDDIR}testlogs/${AssetID}-DIAG.csv" && "$DIAGUpload" != "${TRUE}" ]] && { echo -e "TechnicianUserName: $TechID" >> "${PCDDIR}testlogs/${AssetID}-DIAG.csv"; azure_upload DIAG; }
    [[ "${AssetID}" && "$GRVariablesUpload" != "${TRUE}" && "$GRVariables" == "True" ]] && generate_grvariables_file
    return
}
update_logs() {
    [[ "$(ls /tmp/erase_tmp_files/${SERIAL}* 2>> "${PCDDIR}Logging/tracelog" | wc -l)" != "0" && "$AssetID" ]] && rename.ul "${SERIAL}" "${AssetID}" /tmp/erase_tmp_files/${SERIAL}-WIPE*html
    [[ "$(ls ${PCDDIR}testlogs/${SERIAL}* 2>> "${PCDDIR}Logging/tracelog" | wc -l)" != "0" && "$AssetID" ]] && rename.ul "${SERIAL}" "${AssetID}" ${PCDDIR}testlogs/${SERIAL}* # Assigns asset ID to any outputs completed prior to providing ID
    [[ "$AssetIDOld" && "$(ls ${PCDDIR}testlogs/${AssetIDOld}* 2>> "${PCDDIR}Logging/tracelog" | wc -l)" != "0" ]] && rename.ul "${AssetIDOld}" "${AssetID}" ${PCDDIR}testlogs/${AssetIDOld}* # Assigns asset ID to any outputs completed prior to providing ID
    [[ -e /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}-WIPE-DISK1.html && "$AssetID" ]] && sed -i "s/<\!-- CUSTOM_VALUE(Asset ID) -->*<\/div>/<\!-- CUSTOM_VALUE(Asset ID) -->${AssetID}<\/div>/" /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}*html 2>> "${PCDDIR}Logging/tracelog"
    [[ -e /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}-WIPE-DISK1.html && "$TechID" ]] && sed -i "s/<\!-- CUSTOM_VALUE(Technician ID) -->*<\/div>/<\!-- CUSTOM_VALUE(Technician ID) -->${TechID}<\/div>/" /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}*html 2>> "${PCDDIR}Logging/tracelog"
    return
}
for i in $(ls ${PCDDIR}Variables); do source "${PCDDIR}Variables/${i}"; done
AZURE="http://gridfunc.blob.core.windows.net/input-prod?sv=2017-04-17&ss=bfqt&srt=sco&sp=rwdlacup&se=2018-12-31T22:16:59Z&st=2017-08-25T14:16:59Z&spr=https,http&sig=4FNakR8bhe8PAwxLPhyGjbvCOQ6OB2918v6KHohkT%2BA%3D"
PCDDIR="/usr/local/pcdoctor/bin/"
SERIAL="$(dmidecode --string system-serial-number | awk '{$1=$1;print}')"
HDDCOUNT="$(lsblk --nodeps --include "8,259" --noheadings --output RM | grep --invert-match --ignore-case "1" | wc --lines)"
declare -i Count="0" && until [[ "$Count" == "$HDDCOUNT" ]]; do for i in $(ls /tmp/erase_tmp_files/$(date +%F)*html); do Count+="1"; mv "/tmp/erase_tmp_files/${i}" "/tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}-WIPE-DISK${Count}.html"; done; done
upload_logs
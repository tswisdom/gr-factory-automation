#!/usr/bin/perl
# Copyright PC-Doctor, Inc. All rights reserved.

use strict;
use warnings;

use Switch;
use XML::LibXML;
use Class::Struct;
use Scalar::Util qw(looks_like_number);
use File::Basename;
use DateTime;
use IPC::Cmd qw[can_run run];
use HTML::Entities;
use Getopt::Long;
use Time::Stopwatch;

use constant LICENSEAUTHENTICATIONFAILED => 18;

use constant MAX_FIELD_VALUE_CHARS => 64;

#signal handler: delete temp files, if there are any
$SIG{INT}  = 'IGNORE';
$SIG{TERM} = \&END;
$SIG{HUP} = \&END;
$ENV{MALLOC_CHECK_} = '0';

#HDDInfo : drive information
struct( HDDInfo => {
	idx => '$',
	vendor => '$',
	bus => '$',
	remappedSectors => '$',
	health => '$',
	remappedSectorsPostErase => '$',

	model => '$',
	serial => '$',
	firmware => '$',
	osLocator => '$',
	hwLocator => '$',
	frozen => '$',
	marketedSize => '$',
	sectorCount => '$',
	hpa => '$',
	dco => '$',
	fweraserounds => '$',
	verifyPatterns => '$'
		});

#SystemInfo : general system information
struct( SystemInfo => {
	vendor => '$',
	chassis => '$',
	product => '$',
	serial => '$',
	uuid => '$'
		});

#CustomSysInfo : key/value pair used to store a generic piece of system information
struct( CustomSysInfo => {
	name => '$',
	value => '$'
		});

#AppInfo : information about the PC-Doctor software version
struct( AppInfo => {
	name => '$',
	version => '$'
		});

#TestInfo : information about a specific test run with PC-Doctor
struct( TestInfo => {
	name => '$',
	startDT => '$',
	endDT => '$',
	start => '$',
	end => '$',
	duration => '$',
	result => '$',
	osLocator => '$',
	hwLocator => '$',
	totalPatterns => '$',
		});

#ScriptInfo : information about a specific script run with PC-Doctor
struct( ScriptInfo => {
	description => '$',
	tests => '@'
		});

#CustomField : a custom field name/value pair to be included in the generated reports
struct( CustomField => {
	name => '$',
	value => '$'
		});

#trimWhiteSpace : trim beginning and ending whitespace from string
sub trimWhiteSpace {
	my $value = shift;
	$value=~s/^\s+//;
	$value=~s/\s+$//;
	return $value;
}

#showUsage : help for this workflow script
sub showUsage {
	my $maxsize = 17;
	
	print STDERR "PC-Doctor Drive Erase Usage:\n";
	printf(STDERR " $0 <--cfgdir=configuration directory> <--autoerase> <--forceblkerase> <--editreports> <--help>\n");
	printf(STDERR "    %-*s : %s\n",$maxsize,"--cfgdir", "Specify an alternate configuration directory, default=\"./erase_config\"");
	printf(STDERR "    %-*s : %s\n",$maxsize,"--autoerase", "Noninteractive erase mode. Erase all drives, no prompting, exits with 0 for success");
	printf(STDERR "    %-*s : %s\n",$maxsize,"--forceblkerase", "Run 2nd pass block erase on all drives regardless of whether first pass (FW erase) succeeded. Defaults to false");
	printf(STDERR "    %-*s : %s\n",$maxsize,"--editreports", "Launch menu for editing reports and generating final PDF reports (not compatible with --autoerase)");
	printf(STDERR "    %-*s : %s\n",$maxsize,"--autogenreports","Noninteractive report generation mode. Generate PDF reports automatically, if any HTML reports exist in temporary directory");
	printf(STDERR "    %-*s : %s\n",$maxsize,"--help", "Show this help output\n");
}

my $basedir = `pwd`;
$basedir = trimWhiteSpace($basedir);
my $driveEraseSignatureName = "driveSignature";
my $driveEraseSignatureProgram = "./$driveEraseSignatureName";
my @hdds = ();
my @hdd_names = ();
my @selected_drives;
my $sysInfo = SystemInfo->new();
my $appInfo = AppInfo->new();
#my $memInfo = MemoryInfo->new();
#my @cpus = ();
#my @memoryChips = ();
my %testListHash = ();
my @custom_sysinfo = ();
my %customFieldsHash = ();
my @customFieldsIndices = ();
my %configValuesHash = (
	remote_server_ip => '',
	wipe_dco => 'no',
	wipe_hpa => 'no',
	tmp_dir => '/tmp/erase_tmp_files',
	output_dir => '/tmp/erase_reports',
	template_html => 'report_template.html',
	fw_erase_script => 'scripts/fw_erase_script',
	block_erase_script => 'scripts/block_erase_script',
	fw_verify_percentage => '1',
	attempt_unfreeze => 'yes'
	);
my $cfgdir = "./erase_config";
my $autoerase=0;
my $editreportsonly=0;
my $autogenreports=0;
my $forceblkerase=0;
my $showhelp=0;

#### UPFRONT ARGUMENT CHECKING ####

if ( $< != 0 ) {
	printf(STDERR "This script must be run as root\n"); 
	exit 1;
}
GetOptions ("cfgdir=s" => \$cfgdir,
			"autoerase" => \$autoerase,
			"forceblkerase" => \$forceblkerase,
			"editreports" => \$editreportsonly,
			"autogenreports" => \$autogenreports,
			"help" => \$showhelp,
	);

if ($showhelp) {
	showUsage();
	exit 1;
}


###################START VERIFY INPUTS###################

if ($editreportsonly && $autogenreports) {
	printf(STDERR "The options \"--autogenreports\" and \"--editreports\" cannot be used together\n"); 
	exit 1;
}

my $cfgfile = "$cfgdir/erase_drive.cfg";
if (! -e $cfgfile) {
	printf(STDERR "Missing required configuration file $cfgfile\n"); 
	exit 1;
}

#Check if the system has the required tools to run this script
my @reqcmds = ('smartctl', 'hdparm', 'dd', 'wkhtmltopdf', 'blockdev', 'diff', 'rtcwake', 'uuidgen');
my @missingcmds = ();
for my $cmd (@reqcmds) {
	if (! can_run($cmd)) {
		push(@missingcmds, $cmd);
	}
}
#Check if this script is being run from the same directory as the pcd cmd client
if (! -e "./pcd") {
	push(@missingcmds, "./pcd");
}
#Check if this script is being run from the same directory as the PC-Doctor resourceUtil utility
if (! -e "./resourceUtil") {
	push(@missingcmds, "./resourceUtil");
}
if (! -e "$driveEraseSignatureProgram") {
	push(@missingcmds, "$driveEraseSignatureProgram");
}
if (0 != scalar(@missingcmds)) {
	printf(STDERR "This script requires the following missing commands: " . join(", ", @missingcmds) . "\n");
	exit 1;
}

#try to read in custom values from the configuration file
if ( open my $fh, '<', "$cfgfile" ) {
	while (my $line = <$fh>) {
		if ($line =~ /([^=]+)=(.*)/) {
			my $val1 = trimWhiteSpace($1);
			my $val2 = trimWhiteSpace($2);
			if ($val1 =~ /CustomField.(\d+).Name/) {
				my $customField;
				if (exists($customFieldsHash{$1})) {
					$customField = $customFieldsHash{$1};
				}
				else {
					$customField = CustomField->new(
						name=>'',
						value=>'');
					push(@customFieldsIndices,$1);	    
				}
				$customField->name($val2);
				$customFieldsHash{$1} = $customField;
			}
			elsif ($val1 =~ /CustomField.(\d+).Value/) {
				my $customField;
				if (exists($customFieldsHash{$1})) {
					$customField = $customFieldsHash{$1};
				}
				else {
					$customField = CustomField->new(
						name=>'',
						value=>'');
					push(@customFieldsIndices,$1);		    
				}
				$customField->value($val2);
				$customFieldsHash{$1} = $customField;
			}
			elsif (exists $configValuesHash{$val1}) {
				$configValuesHash{$val1} = $val2;
			}
		}
	}
	close($fh);
}

my $tmpdir = $configValuesHash{'tmp_dir'};
my $remote_server_ip = $configValuesHash{'remote_server_ip'};
$remote_server_ip =~ s/\s+//g; #strip out spaces
my $ret = system("mkdir -p $tmpdir");
if ( ! -e $tmpdir ) {
	printf(STDERR "Cannot create temp directory $tmpdir\n"); 
	exit 1;
}
my $sysinfo_tmp_file = "$tmpdir/sysinfo.xml";
my $pcd_output_file = "$tmpdir/pcd_output_file.txt";
my $testlist_tmp_file = "$tmpdir/testlist.txt";
my $outdir = $configValuesHash{'output_dir'};
$ret = system("mkdir -p $outdir");
if ( ! -e $outdir ) {
	printf(STDERR "Cannot create outout directory $outdir\n"); 
	exit 1;
}
my $template_html_rel = $configValuesHash{'template_html'};
my $template_html = "$cfgdir/" . $template_html_rel;

if ( ! -e $template_html ) {
	printf(STDERR "Cannot find template HTML file \"$template_html\"\n"); 
	exit 1;
}

$ret = system("cp -rdpf $cfgdir/* $tmpdir/");
if ($ret) {
	printf(STDERR "Cannot copy configuration files to temp directory \"$tmpdir\"\n"); 
	exit 1;
}
my $fw_erase_script = $configValuesHash{'fw_erase_script'};

if ( ! -e $fw_erase_script ) {
	printf(STDERR "Cannot find firmware erase script \"$fw_erase_script\"\n"); 
	exit 1;
}
my $block_erase_script = $configValuesHash{'block_erase_script'};
if ( ! -e $fw_erase_script ) {
	printf(STDERR "Cannot find block erase script \"$block_erase_script\"\n"); 
	exit 1;
}

my $drivePercentage = $configValuesHash{'fw_verify_percentage'};
$drivePercentage =~ s/\s+//g;
if ( $drivePercentage !~ /^(?:\d+\.?|\.\d)\d*\z/ ) {
	printf(STDERR "fw_verify_percentage is not a valid decimal number > 0 and <= 100\n");
	exit 1;
}
$drivePercentage = $drivePercentage + 0; #implicit conversion to number
if ($drivePercentage > 100.0 ||
    0 == $drivePercentage) {
	printf(STDERR "fw_verify_percentage is not a valid decimal number > 0 and <= 100\n");
	exit 1;
}

#log_mesg : log to file as well as to the screen
#logs to stdout or stderr based on argument #2
#argument #3 set to zero means don't log to screen
sub log_mesg {
	my $log_file = "$tmpdir/erase_log_$$.log";
	if (open(my $fh, ">>", $log_file)) {
		print $fh $_[0];
		close($fh);
	}	
	if (defined $_[2] &&
		0 == $_[2]) {
		return;
	}
	if (defined $_[1] &&
		$_[1]) {
		print STDERR $_[0];
	}
	else {
		print STDOUT $_[0];
	}
}
###################END VERIFY INPUTS###################


#import_drive_wipe_override : make sure drive wipe can be run on SSDs by overriding settings
sub import_drive_wipe_override {
	my $filename = shift;
	my $value = shift;
	if (open(my $fh, ">", $filename)) {
		print $fh $value;
		system("./resourceUtil import $filename 1>/dev/null 2>&1");
		close($fh);
	}
}
#import_drive_wipe_overrides : make sure drive wipe can be run on SSDs by overriding settings
sub import_drive_wipe_overrides {
	import_drive_wipe_override("libAtaInfo.p5i", "EnableDriveWipeForSSDs.value=true");
	import_drive_wipe_override("libScsiInfo.p5i", "EnableDriveWipeForSSDs.value=true");
	import_drive_wipe_override("pcdrsysinfostorage.p5i", "EnableDriveWipeForNVMe.value=true");
}

#find_device_name : PC-Doctor XML SysInfo : return device name
sub find_device_name {
	my $node = shift;
	for my $child ($node->childNodes) {
		if ($child->nodeType() != XML_ELEMENT_NODE) {next;}
		if ($child->nodeName eq "Name") {
			return $child->to_literal();
		}
	}
	return "";	
}

#find_property_values_by_key : PC-Doctor XML SysInfo : return property value by key
sub find_property_values_by_key {
	my @values = ();
	my $node = shift;
	my $prop_key = shift;
	for my $child ($node->childNodes) {
		#print $child->nodeName() . "...\n";
		#my @nodes = $child->findnodes(".//pcd:Property/pcd:Key[text( )='ATAModelNumber/../pcd:Value']");
		my @nodes = $child->findnodes(".//pcd:Key[text( )='$prop_key']/../pcd:Value");
		for my $valueNode (@nodes) {
			push(@values, $valueNode->to_literal());
		}
		if (0==scalar(@values) && $child->nodeName eq "Property") {
			push(@values, find_property_values_by_key($child, $prop_key));
		}
	}
	return @values;
}

#find_property_values_by_category : PC-Doctor XML SysInfo : return property value by category
#returns multiple results for a property of type string array
sub find_property_values_by_category {
	my @values = ();
	my $node = shift;
	my $prop_cat = shift;

	for my $child ($node->childNodes) {
		if ($child->nodeType() != XML_ELEMENT_NODE) {next;}
		if ( ! $child->hasAttribute('category')) {next;}
		my @categories = split('\|', $child->getAttribute('category'));
		if ( grep( /^$prop_cat$/, @categories ) ) {
			my @nodes = $child->findnodes(".//pcd:Value");
			for my $valueNode (@nodes) {
				push(@values, $valueNode->to_literal());
			}
		}
		if (0==scalar(@values) && $child->nodeName eq "Property") {
			push(@values, find_property_values_by_category($child, $prop_cat));
		}
	}
	return @values;
}

#find_property_by_key : PC-Doctor XML SysInfo : return property value by key
sub find_property_by_key {
	my @array = find_property_values_by_key($_[0], $_[1]);
	if (scalar(@array)) {
		return $array[0];
	}
	return "";
}
#find_properties_by_key : PC-Doctor XML SysInfo : return property value by key
#returns multiple results for a property of type string array
sub find_properties_by_key {
	my @array = find_property_values_by_key($_[0], $_[1]);
	return @array;
}
#find_property_by_category : PC-Doctor XML SysInfo : return property value by category
sub find_property_by_category {
	my @array = find_property_values_by_category($_[0], $_[1]);
	if (scalar(@array)) {
		return $array[0];
	}
	return "";
}

#find_drive_model_number : PC-Doctor XML SysInfo : get HDD model number by key
sub find_drive_model_number {
	my $model = find_property_by_category($_[0], "Model");
	if (0 == length($model)) {
		$model = find_property_by_key($_[0], "ATAModelNumber");
	}
	if (0 == length($model)) {
		$model = find_property_by_key($_[0], "SCSIProductId");
	}
	if (0 == length($model)) {
		$model = find_property_by_key($_[0], "NVMeModel");
	}
	if (0 == length($model)) {
		$model = find_property_by_key($_[0], "Model");
	}
	return $model;
}
#find_drive_vendor : PC-Doctor XML SysInfo : get HDD vendor by key
sub find_drive_vendor {
	my $vendor = find_property_by_key($_[0], "ATAVendor");
	if (0 == length($vendor)) {
		$vendor = find_property_by_key($_[0], "SCSIVendorName");
	}
	if (0 == length($vendor)) {
		$vendor = find_property_by_key($_[0], "NVMeVendorName");
	}
	return $vendor;
}
#find_drive_block_count : return block count
sub find_drive_block_count {
	${$_[1]} = `blockdev --getsz $_[0]`;
	${$_[1]} =~ s/\s+//g;
	${$_[2]} = ${$_[1]};
	${$_[3]} = `blockdev --getss $_[0]`;
	${$_[3]} =~ s/\s+//g;
	if (${$_[1]} !~ /[0-9]+/ || ${$_[3]} !~ /[0-9]+/) {
		${$_[1]}=0;
		${$_[2]}=0;
		${$_[3]}=0;
		return;
	}
	if (${$_[1]} > 512) {
		${$_[1]} = ${$_[1]} / int(${$_[3]}/512); #convert to logical sector size
	}
}
#find_drive_block_count_str : return string repr of block count
sub find_drive_block_count_str {
	my $blkcount=0;
	my $blkcount512=0;
	my $blksize=0;
	find_drive_block_count($_[0], \$blkcount, \$blkcount512, \$blksize);
	return "$blkcount (${blksize} byte sector)";
}
#find_drive_size : PC-Doctor XML SysInfo : get HDD size by key
sub find_drive_size {
	my $size = find_property_by_key($_[0], "ATADiskSizeMarket");
	if (0 == length($size)) {
		$size = find_property_by_key($_[0], "MarketedSize");
	}
	if (0 == length($size)) {
		$size = find_property_by_key($_[0], "NamespaceMarketedSize");
	}
	if (0 == length($size)) {
		$size = find_property_by_key($_[0], "MemorySize");
	}
	return $size;
}
#find_drive_serial_number : PC-Doctor XML SysInfo : get HDD serial by key
sub find_drive_serial_number {
	my $serial = find_property_by_category($_[0], "Serial");
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "ATASerialNumber");
	}
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "SCSIVPDUnitSerial");
	}
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "NVMeSerialNum");
	}
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "SerialNumber");
	}
	return $serial;
}
#find_drive_firmware_version : PC-Doctor XML SysInfo : get HDD FW version
sub find_drive_firmware_version {
	my $serial = find_property_by_category($_[0], "Firmware");
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "ATAFirmwareRev");
	}
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "SCSIProductRev");
	}
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "NVMeFirmwareRev");
	}
	if (0 == length($serial)) {
		$serial = find_property_by_key($_[0], "FirmwareRevision");
	}
	return $serial;
}
#find_bus : PC-Doctor XML SysInfo : get HDD bus by PC-Doctor capability
sub find_bus {
	for my $child ($_[0]->childNodes) {
		if ($child->nodeName eq "Capability") {
			if ($child->to_literal() eq "ATA") {
				return "ATA";
			}
			elsif ($child->to_literal() eq "SAS") {
				return "SAS";
			}
			elsif ($child->to_literal() eq "NVME") {
				return "NVMe";
			}
			elsif ($child->to_literal() eq "MMCSDFlash") {
				return "MMC";
			}
		}
	}
}
#find_health : use smartctl exit code to estimate drive health
sub find_health {
	system("smartctl -H $_[0] 1>/dev/null 2>&1");
	my $ret = $? >> 8;
	if (($ret & 0x01) == 0x0) {
		if ($ret & (1<<3)) {
			return "failing";
		}
		if ($ret & (1<<4)) {
			return "threshold error";
		}
		if ($ret & (1<<6)) {
			return "device log error";
		}
		if ($ret & (1<<7)) {
			return "self-test log error";
		}
		return "good";
	}
	else {
		return "unknown";
	}
}

#find_remapped_sectors : try to use smartctl to get remapped sectors (works for ATA and SAS)
sub find_remapped_sectors {
	if (open(my $fh, '-|', "smartctl -A $_[0]")) {
		while (my $line = <$fh>) {
			if ($line =~ /5 Reallocated_Sector_Ct\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\d+)/) {
				return $1;
			}
			elsif ($line =~ /Elements in grown defect list:\s+(\d+)\s*/) {
				return $1;
			}
		}
		close($fh);
	}
	return "0";
}

#find_child_node_value : PC-Doctor XML SysInfo : return the value of child node by name
sub find_child_node_value {
	for my $child ($_[0]->childNodes) {
		if ($child->nodeName eq $_[1]) {
			return $child->to_literal();
		}
	}
	return "";
}
#is_drive_frozen : PC-Doctor XML SysInfo : get frozen status from property key
sub is_drive_frozen {
	my @values = find_property_values_by_key($_[0], "ATASecurityStatus");
	for my $value (@values) {
		if ($value eq "Security is frozen") {
			return 1;
		}
	}
	return 0;
}

#msgbox : dialog OK msgbox
sub msgbox {
	my @args = ('dialog', '--title', $_[0], '--msgbox', $_[1], '0', '0');
	system(@args);
}

#msgbox_ok_cancel : dialog OK/cancel msgbox
sub msgbox_ok_cancel {
	#my @args = ('dialog', '--title', $_[0], '--no-collapse', '--yes-label', 'Commit Changes', '--no-label', 'Go Back', '--yesno', $_[1], '0', '0');
	my @args = ('dialog', '--title', $_[0], '--no-collapse', '--yes-label', $_[2], '--no-label', $_[3], '--yesno', $_[1], '0', '0');
	system(@args);
	my $ret = $? >> 8;
	return $ret;
}

#msgbox_yes_no_cancel : dialog yes/no/cancel msgbox
sub msgbox_yes_no_cancel {
	my @args = ('dialog', '--title', $_[0], '--ok-label', 'Yes', '--cancel-label', 'Cancel', '--extra-button', '--extra-label', 'No', '--yesno', $_[1], '0', '0');
	system(@args);
	my $ret = $? >> 8;
	return $ret;
}

#show_menu : dialog menu specifically for multiselect of HDDs
sub show_menu {
	my @output = ();
	my @selections;
	push(@selections, "0 \"ALL DRIVES\" off");
	push(@selections, @{$_[0]});
	#dialog with option --checklist writes the selection to STDERR, that is why we are doing the following
	no warnings 'once';
	open(CPERR, ">&STDERR");
	open(STDERR, ">$tmpdir/input.$$");
	my @args = ('dialog', @{$_[1]}, $_[2], 0, 0, 0, @selections);
	#print join(" ", @args);
	#print "myargs = " . @args . "\n";
	system(join(" ", @args));
	my $ret = $? >> 8;
	if ($ret > 0) {
		push(@output, "c"); return @output;
	}
	my $FILE;
	if (!open(FILE, "$tmpdir/input.$$")) {
		push(@output, "c"); return @output;
	}
	chomp(@output = <FILE>);
	close(FILE);
	close(STDERR);
	open(STDERR, ">&CPERR");
	return @output;

}
#show_menu2 : dialog menu for any generic data passed in as an array
sub show_menu2 {

	#create menu from device list
	my @menu_list = ();
	my $counter=1;

	if (defined $_[4] && length($_[4])) {
		push(@menu_list, 0);
		push(@menu_list, $_[4]);
	}
	
	for my $value (@{$_[0]}) {
		$value =~ s/^\s+|\s+$//g;
		if (length($value) > 0) {
			push(@menu_list, $counter);
			$counter++;
			push(@menu_list, $value);
		}
	}
	if (0 == scalar(@menu_list)) {
		push(@menu_list, 'NO ' . $_[3]);
		push(@menu_list, 'DETECTED...');
	}
	
	#dialog with option --menu writes the selection to STDERR, that is why we are doing the following
	open(CPERR, ">&STDERR");
	open(STDERR, ">/tmp/input.$$");
	my @args = ('dialog', @{$_[1]}, $_[2], 0, 0, scalar(@menu_list), @menu_list);
	system(@args);
	my $ret = $? >> 8;
	switch($ret) {
		case 1 { return 'c'; }
		case 2 { return 'a'; }
		case 3 {
			if (defined $_[5] ) {
				${$_[5]} = 1;
			}
		}
	}
	my $FILE;
	if (!open(FILE, "/tmp/input.$$")) {
		return 0;
	}
	$ret = join("", <FILE>);
	close(FILE);
	close(STDERR);
	open(STDERR, ">&CPERR");
	return $ret;
}
#process_system : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from SMBIOS
sub process_system {
	$sysInfo->vendor(find_property_by_key($_[0], "t1_manufacturer"));
	$sysInfo->product(find_property_by_key($_[0], "t1_product"));
	$sysInfo->serial(find_property_by_key($_[0], "t1_serial"));
	$sysInfo->uuid(find_property_by_key($_[0], "t1_uuid")); 
}
#process_chassis : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from SMBIOS
sub process_chassis {
	$sysInfo->chassis(find_property_by_key($_[0], "t3_type_msk7_srt0"));
}
#process_cpu : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from CPU device
sub process_cpu {
	my @cpuInfo = ();
	push(@cpuInfo, CustomSysInfo->new( name=>'header', value=>'Processor'));
	push(@cpuInfo, CustomSysInfo->new( name=>'', value=>find_device_name($_[0])));
	push(@cpuInfo, CustomSysInfo->new( name=>'', value=>find_property_by_key($_[0], "VendorID")));
	push(@cpuInfo, CustomSysInfo->new( name=>'Cores', value=>find_property_by_key($_[0], "Cores")));
	push(@cpuInfo, CustomSysInfo->new( name=>'Stepping', value=>find_property_by_key($_[0], "Stepping")));
	push(@cpuInfo, CustomSysInfo->new( name=>'Current Speed', value=>find_property_by_key($_[0], "CurrentSpeed")));
	#TODO add external clock from SMBIOS if possible/feasible
	#TODO add voltage from SMBIOS if possible/feasible
	push(@custom_sysinfo, \@cpuInfo);
}
#process_memoryChip : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from MemoryChip device
sub process_memoryChip {
	my @memChipInfo = ();
	push(@memChipInfo, CustomSysInfo->new( name=>'header', value=>'Memory Bank'));
	push(@memChipInfo, CustomSysInfo->new( name=>'', value=>find_property_by_key($_[0], "MemoryManufacturer")));
	push(@memChipInfo, CustomSysInfo->new( name=>'', value=>find_property_by_key($_[0], "MemoryModuleSize")));
	push(@memChipInfo, CustomSysInfo->new( name=>'', value=>find_property_by_key($_[0], "MemoryType")));
	push(@memChipInfo, CustomSysInfo->new( name=>'', value=>find_property_by_key($_[0], "MemoryModuleType")));
	push(@memChipInfo, CustomSysInfo->new( name=>'', value=>find_property_by_key($_[0], "MemorySerialNumber")));
	#TODO add from SMBIOS if possible/feasible [type detail]
	push(@custom_sysinfo, \@memChipInfo);
}
#process_memory : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from Memory device
sub process_memory {
	my @memoryInfo = ();
	push(@memoryInfo, CustomSysInfo->new( name=>'header', value=>'Memory'));
	push(@memoryInfo, CustomSysInfo->new( name=>'', value=>find_property_by_key($_[0], "TotalPhysicalMemory")));
	push(@memoryInfo, CustomSysInfo->new( name=>'Maximum Physical Memory', value=>find_property_by_key($_[0], "MaxPhysicalMemory")));
	#TODO add free memory banks count
	push(@custom_sysinfo, \@memoryInfo);
}

#process_pci_card : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from PCI device
sub process_pci_card {
	#my @pciSysInfoArray = @{$_[1]};
	my $deviceName = find_device_name($_[0]);
	my $vendorId = find_property_by_key($_[0], "PCI_VendorID");
	my $deviceId = find_property_by_key($_[0], "PCI_DeviceID");
	push(@{$_[1]}, CustomSysInfo->new( name=>'', value=>sprintf("%s (%s:%s)", $deviceName, $vendorId, $deviceId)));
}
#process_graphics_card : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from VideoCard device
sub process_graphics_card {
	my @pciInfo = ();
	push(@pciInfo, CustomSysInfo->new( name=>'header', value=>'Graphics Card'));
	process_pci_card($_[0], \@pciInfo);
	push(@custom_sysinfo, \@pciInfo);
}
#process_sound_card : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from SoundCard device
sub process_sound_card {
	my @pciInfo = ();
	push(@pciInfo, CustomSysInfo->new( name=>'header', value=>'Sound Card'));
	process_pci_card($_[0], \@pciInfo);
	push(@custom_sysinfo, \@pciInfo);
}
#process_storage_controller : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from MassStorageController device
sub process_storage_controller {
	my @pciInfo = ();
	push(@pciInfo, CustomSysInfo->new( name=>'header', value=>'Storage Controller'));
	process_pci_card($_[0], \@pciInfo);
	push(@custom_sysinfo, \@pciInfo);
}
#process_network_card : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from NetworkCard device
sub process_network_card {
	my @pciInfo = ();
	push(@pciInfo, CustomSysInfo->new( name=>'header', value=>'Network Adapter'));
	process_pci_card($_[0], \@pciInfo);
	push(@pciInfo, CustomSysInfo->new( name=>'MAC', value=>find_property_by_key($_[0], "MACAddress")));
	push(@pciInfo, CustomSysInfo->new( name=>'IP', value=>find_property_by_key($_[0], "IPAddress")));
	push(@custom_sysinfo, \@pciInfo);
}
#process_optical_drive : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from Optical device
sub process_optical_drive {
	my $vendor = find_drive_vendor($_[0]);
	my $model = find_drive_model_number($_[0]);
	my $serial = find_drive_serial_number($_[0]);
	my @profiles = find_properties_by_key($_[0], "MMC_SupportedProfiles");
	my @opticalInfo = ();
	push(@opticalInfo, CustomSysInfo->new( name=>'header', value=>'Optical Drive'));
	push(@opticalInfo, CustomSysInfo->new( name=>'', value=>$vendor));
	push(@opticalInfo, CustomSysInfo->new( name=>'', value=>$model));
	push(@opticalInfo, CustomSysInfo->new( name=>'Serial', value=>$serial));
	push(@opticalInfo, CustomSysInfo->new( name=>'Profiles', value=>join(" ", @profiles)));
	push(@custom_sysinfo, \@opticalInfo);	
}
#process_motherboard : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from SystemBoard device
sub process_motherboard {
	my @motherboardInfo = ();
	push(@motherboardInfo, CustomSysInfo->new( name=>'header', value=>'Motherboard'));
	push(@motherboardInfo, CustomSysInfo->new( name=>'Manufacturer', value=>find_property_by_key($_[0], "t2_manufacturer")));
	push(@motherboardInfo, CustomSysInfo->new( name=>'Product', value=>find_property_by_key($_[0], "t2_product")));
	push(@motherboardInfo, CustomSysInfo->new( name=>'Version', value=>find_property_by_key($_[0], "t2_ver")));
	push(@motherboardInfo, CustomSysInfo->new( name=>'Serial', value=>find_property_by_key($_[0], "t2_serial")));
	push(@custom_sysinfo, \@motherboardInfo);
}
#process_bios : custom sysinfo for report : PC-Doctor XML SysInfo : gathered from BIOS device
sub process_bios {
	my @biosInfo = ();
	push(@biosInfo, CustomSysInfo->new( name=>'header', value=>'BIOS'));
	push(@biosInfo, CustomSysInfo->new( name=>'Vendor', value=>find_properties_by_key($_[0], "t0_vendor")));
	push(@biosInfo, CustomSysInfo->new( name=>'Version', value=>find_properties_by_key($_[0], "t0_ver")));
	push(@biosInfo, CustomSysInfo->new( name=>'Release Date', value=>find_properties_by_key($_[0], "t0_date")));
	push(@custom_sysinfo, \@biosInfo);
	my @biosFeaturesInfo = ();
	push(@biosFeaturesInfo, CustomSysInfo->new( name=>'header', value=>'BIOS Features'));
	my @characteristics = find_properties_by_key($_[0], "t0_characteristics");
	push(@biosFeaturesInfo, CustomSysInfo->new( name=>'', value=>join(", ", @characteristics)));
	push(@custom_sysinfo, \@biosFeaturesInfo);
}
#process_serial_port: custom sysinfo for report : PC-Doctor XML SysInfo : gathered from Serial Port
sub process_serial_port {
	my @serialPortInfo = ();
	push(@serialPortInfo, CustomSysInfo->new( name=>'header', value=>'Serial Port'));
	push(@serialPortInfo, CustomSysInfo->new( name=>'', value=>find_device_name($_[0])));
	push(@serialPortInfo, CustomSysInfo->new( name=>'Port Number', value=>find_property_by_key($_[0], "Port")));
	push(@serialPortInfo, CustomSysInfo->new( name=>'IRQ', value=>find_property_by_key($_[0], "SerialPortIRQLine")));
	push(@custom_sysinfo, \@serialPortInfo);
}
#process_parallel_port: custom sysinfo for report : PC-Doctor XML SysInfo : gathered from Parallel Port
sub process_parallel_port {
	my @parallelPortInfo = ();
	push(@parallelPortInfo, CustomSysInfo->new( name=>'header', value=>'Parallel Port'));
	push(@parallelPortInfo, CustomSysInfo->new( name=>'', value=>find_device_name($_[0])));
	push(@parallelPortInfo, CustomSysInfo->new( name=>'Base Address', value=>find_property_by_key($_[0], "BaseAddress")));
	push(@parallelPortInfo, CustomSysInfo->new( name=>'IRQ', value=>find_property_by_key($_[0], "ParallelPortIRQLine")));
	push(@custom_sysinfo, \@parallelPortInfo);
}
#process_usb_internal_hub: custom sysinfo for report : PC-Doctor XML SysInfo : gathered from USB Internal Hubs
sub process_usb_internal_hub {
	my @usbInternalHubInfo = ();
	push(@usbInternalHubInfo, CustomSysInfo->new( name=>'header', value=>'USB Internal Hub'));
	push(@usbInternalHubInfo, CustomSysInfo->new( name=>'', value=>find_device_name($_[0])));
	push(@usbInternalHubInfo, CustomSysInfo->new( name=>'Port Count', value=>find_property_by_key($_[0], "PortCount")));
	push(@custom_sysinfo, \@usbInternalHubInfo);
}

#addCustomSysinfoType : custom sysinfo for report : this subroutine actually outputs the HTML for each custom sysinfo device of a given type
sub addCustomSysinfoType {
	my $outValue="";
	for my $sysinfoDevice (@custom_sysinfo) {
		my @currArray = @{$sysinfoDevice};
		if (scalar(@currArray) &&
			($currArray[0]->value ne $_[0])) {
			next; #skip if this is not the requested type
		}
		my $numFieldsAdded=0;
		my $fullRowAdded=0;
		for (my $idx=0; $idx < scalar(@currArray); $idx++) {
			if (length($currArray[$idx]->value)) {
				$numFieldsAdded++;
				if (0 == $idx) {
					$outValue .= "<div class=\"label\">" . $currArray[$idx]->value . ":</div>\n";
				}
				else {
					if (0 == $fullRowAdded) {
						$outValue .= "<div class=\"data fullrow\">";
						$fullRowAdded=1;
					}
					else {
						$outValue .= ", ";
					}
					if (length($currArray[$idx]->name)) {
						$outValue .= $currArray[$idx]->name . ": ";
					}
					$outValue .= $currArray[$idx]->value;
				}
			}
		}
		if ($numFieldsAdded > 1 && length($outValue)) { $outValue .= "</div>\n"; }
	}
	return $outValue;
}

#addCustomSysinfo : custom sysinfo for report : this subroutine outputs the custom sysinfo HTML in particular order by device type
sub addCustomSysinfo {
	my $outValue="";
	$outValue .= addCustomSysinfoType('Processor');
	$outValue .= addCustomSysinfoType('Memory Bank');
	$outValue .= addCustomSysinfoType('Memory');
	$outValue .= addCustomSysinfoType('Graphics Card');
	$outValue .= addCustomSysinfoType('Sound Card');
	$outValue .= addCustomSysinfoType('Storage Controller');
	$outValue .= addCustomSysinfoType('Disk');
	$outValue .= addCustomSysinfoType('Network Adapter');
	$outValue .= addCustomSysinfoType('Optical Drive');
	$outValue .= addCustomSysinfoType('Motherboard');
	$outValue .= addCustomSysinfoType('BIOS');
	$outValue .= addCustomSysinfoType('BIOS Features');
	$outValue .= addCustomSysinfoType('Serial Port');
	$outValue .= addCustomSysinfoType('Parallel Port');
	$outValue .= addCustomSysinfoType('USB Internal Hub');
	return $outValue;
}

#is_mounted_at_location : detect if a drive is mounted (drive must be unmounted to be erased)
sub is_mounted_at_location {
	if (! defined $_[0])  {
		return 0;
	}
	if (! -e $_[0]) {
		return 0;
	}
	my $mntpoint = $_[0];
	my($filename, $dirs, $suffix) = fileparse($_[1]);
	return `df -h $mntpoint | grep -c $filename`;
}

sub sanitize_quotes {
	my $value = shift;
	$value =~ s/'/\'/g;
	$value =~ s/"/\"/g;
	return $value;
}

#process_hdd : detect HDD sysinfo and current status from PC-Doctor SysInfo and Linux utilities
sub process_hdd {
	my $bus = find_bus($_[0]);
	my $vendor = find_drive_vendor($_[0]);
	my $model = sanitize_quotes(find_drive_model_number($_[0]));
	my $serial = sanitize_quotes(find_drive_serial_number($_[0]));
	my $marketed_size = find_drive_size($_[0]);
	my $firmware = sanitize_quotes(find_drive_firmware_version($_[0]));
	my $os_locator = find_child_node_value($_[0],"OS_Locator");
	my $hw_locator = find_child_node_value($_[0],"HW_Locator");
	my $security_frozen = is_drive_frozen($_[0]);
	if (length $os_locator) {
		my $block_count = find_drive_block_count_str($os_locator);
		my $health = find_health($os_locator);
		#check for certain mount location we don't want to list as erasable
		my $mountedAtLocation = is_mounted_at_location('/', $os_locator) || is_mounted_at_location($ENV{ 'KEYBASEDIR' }, $os_locator);
		if ( 0 == $mountedAtLocation ) {
			my $remappedSectors = find_remapped_sectors($os_locator);
			my $hdd = HDDInfo->new();
			$hdd->sectorCount($block_count);
			$hdd->marketedSize($marketed_size);
			$hdd->bus($bus);
			$hdd->vendor($vendor);
			$hdd->model($model);
			$hdd->serial($serial);
			$hdd->firmware($firmware);
			$hdd->osLocator($os_locator);
			$hdd->hwLocator($hw_locator);
			$hdd->frozen($security_frozen);
			$hdd->remappedSectors($remappedSectors);
			$hdd->health($health);
			$hdd->hpa("Doesn't exist");
			$hdd->dco("Doesn't exist");
			$hdd->fweraserounds(0);
			$hdd->verifyPatterns(1);
			my $name = " \'$model - $serial ($firmware)- $os_locator (frozen = " . $security_frozen . ")\' off";
			if (scalar(@hdds)) {
				if (($hdd->osLocator cmp $hdds[scalar(@hdds)-1]->osLocator) < 0) {
					unshift @hdds, $hdd;
					unshift @hdd_names, $name;
					return;
				}
			}
			push(@hdds, $hdd);
			push(@hdd_names, $name);
		}
		
		#add sysinfo for report
		my @hddInfo = ();
		push(@hddInfo, CustomSysInfo->new( name=>'header', value=>'Disk'));
		push(@hddInfo, CustomSysInfo->new( name=>'', value=>$vendor));
		push(@hddInfo, CustomSysInfo->new( name=>'', value=>$model));
		push(@hddInfo, CustomSysInfo->new( name=>'Firmware', value=>$firmware));
		push(@hddInfo, CustomSysInfo->new( name=>'Serial', value=>$serial));
		push(@hddInfo, CustomSysInfo->new( name=>'', value=>sprintf("%s / %s sectors",$marketed_size, $block_count)));
		push(@hddInfo, CustomSysInfo->new( name=>'Bus', value=>$bus));
		push(@hddInfo, CustomSysInfo->new( name=>'Health', value=>$health));
		push(@custom_sysinfo, \@hddInfo);
	}
}
#process_node : process PC-Doctor SysInfo types and dispatch so device specific info can be fetched
sub process_node {
	my $node = shift;

	for my $child ($node->childNodes) {
		if ($child->nodeName eq "Capability") {
			if ($child->to_literal() eq "HardDrive" ||
				$child->to_literal() eq "MMCSDFlash") {
				#print "Found HardDrive or MMCFlash\n";
				process_hdd($node);
			}
			elsif ($child->to_literal() eq "System") {
				#print "Found System\n";
				process_system($node);
			}
			elsif ($child->to_literal() eq "Chassis") {
				#print "Found Chassis\n";
				process_chassis($node);
			}
			elsif ($child->to_literal() eq "CPU") {
				#print "Found CPU\n";
				process_cpu($node);
			}
			elsif ($child->to_literal() eq "MemoryChip") {
				#print "Found MemoryChip\n";
				process_memoryChip($node);
			}
			elsif ($child->to_literal() eq "Memory") {
				#print "Found Memory\n";
				process_memory($node);
			}
			elsif ($child->to_literal() eq "VideoCard") {
				#print "Found VideoCard\n";
				process_graphics_card($node);
			}
			elsif ($child->to_literal() eq "SoundCard") {
				#print "Found SoundCard\n";
				process_sound_card($node);
			}
			elsif ($child->to_literal() eq "MassStorageController") {
				#print "Found MassStorageController\n";
				process_storage_controller($node);
			}
			elsif ($child->to_literal() eq "NetworkCard") {
				#print "Found NetworkCard\n";
				process_network_card($node);
			}
			elsif ($child->to_literal() eq "Optical") {
				#print "Found Optical Drive\n";
				process_optical_drive($node);
			}
			elsif ($child->to_literal() eq "SystemBoard") {
				#print "Found System Board\n";
				process_motherboard($node);
			}
			elsif ($child->to_literal() eq "BIOS") {
				#print "Found BIOS\n";
				process_bios($node);
			}
			elsif ($child->to_literal() eq "SerialPort") {
				#print "Found Serial Port\n";
				process_serial_port($node);
			}
			elsif ($child->to_literal() eq "ParallelPort") {
				#print "Found Parallel Port\n";
				process_parallel_port($node);
			}
			elsif ($child->to_literal() eq "InternalHub") {
				#print "Found USB Internal Hub Port\n";
				process_usb_internal_hub($node);
			}
		}
		elsif ($child->nodeName eq "Device") {
			#recurse to child nodes
			process_node($child);
		}
	}
}

#unmount_drives : try to unmount all drives to be erased to ensure that they are in a good state for erasure
sub unmount_drives {
	for my $selection (@selected_drives) {
		for (my $i=1; $i<11; $i++) {
			my $sublocator = $hdds[$selection]->osLocator . "$i";
			my @args = ('grep', '-q', $sublocator, '/proc/mounts');
			system(@args);
			my $ret=$?;
			if ($ret == 0) {
				log_mesg(sprintf("Drive is mounted \"%s\" attempting to unmount...", $hdds[$selection]->osLocator),1);
				@args = ('umount', $sublocator);
				system(@args);
				$ret=$?;
				if ($ret != 0) {
					log_mesg(sprintf("Unable to unmount \"%s\"\n", $sublocator),1);
					return 0;
				}
				log_mesg("Done\n");
				
			}
		}
	}
	return 1;
}

#is_drive_frozen : PC-Doctor XML SysInfo : get frozen status from hdparm
sub is_drive_frozen_hdparm {
	my $osLocator = ${$_[0]}->osLocator;
	my $frozen_drive=0;
	if (${$_[0]}->bus eq "ATA") {
		my @output = `hdparm -I $osLocator | grep frozen`;
		foreach my $line (@output) {
			if ($line !~ /not/) {
				$frozen_drive=1;
			}
		}
	}
	return $frozen_drive;
}

#search_for_frozen_drives : look through all drives to be erased to see if any are frozen
sub search_for_frozen_drives {
	my $frozen_drives=0;
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			if ($hdds[$selection]->bus eq "ATA") {
				if (is_drive_frozen_hdparm(\$hdds[$selection])) {
					$frozen_drives=1;
					log_mesg(sprintf(" frozen drive selected : %s\n", $hdds[$selection]->osLocator),1);
				}
			}
		}
	}
	return $frozen_drives;
}

#hpa_max_value : get HPA state and the MAX LBA value
sub hpa_get_state {
	my $locator = $_[0];
	my @output = `hdparm -N $locator`;
	foreach my $line (@output) {
		if ($line =~ /\s*max sectors\s*=\s*(\d+)\/(\d+),\s*HPA is (\w+)/) {
			${$_[1]} = $2;
			return $3;
		}
	}
	return "unknown";
}

#hpa_enabled_max_value : get the MAX LBA value. Does not match current LBA size if HPA is enabled
sub hpa_enabled_max_value {
	my $locator = $_[0];
	my $maxValue=0;
	my $state = hpa_get_state($_[0], \$maxValue);
	if ($state eq "enabled") {
		return $maxValue;
	}
	return 0;
}

#search_for_inaccesible_hpas : look through drives, check if the HPAs are accessible
sub search_for_inaccessible_hpas {
	my $markNodes = $_[0];
	my $inaccessibleHPAs=0;
	my $should_wipe = $configValuesHash{'wipe_hpa'};
	if ( defined $should_wipe &&
		 $should_wipe =~ /^[YyTt]/) {
		log_mesg("Checking for HPA regions\n");
	}
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			if ($hdds[$selection]->bus eq "ATA") {
				if ( defined $should_wipe &&
					 $should_wipe =~ /^[YyTt]/) {
					my $blkcount=0;
					my $blkcount512=0;
					my $blksize=0;
					my $maxLBA=0;
					my $state = hpa_get_state($hdds[$selection]->osLocator, \$maxLBA);
					find_drive_block_count($hdds[$selection]->osLocator, \$blkcount, \$blkcount512, \$blksize);
					#the reported sector size for hdparm -N is unclear, so we check
					#it against the logical sector size and a 512-byte sector size
					#when determining if there is an inaccessible HPA region
					if ($maxLBA > 0 &&
						$blkcount > 0 &&
						$blkcount512 > 0 &&
						$maxLBA > $blkcount &&
						$maxLBA > $blkcount512) {
						$inaccessibleHPAs=1;
						if ($markNodes &&
							$hdds[$selection]->hpa eq "Erased") {
							$hdds[$selection]->hpa("Cannot Erase");
						}
					}
				}
			}
		}
	}
	return $inaccessibleHPAs;
}

#wipe_hpas : use hdparm to remove the HPA
sub wipe_hpas {
	my $should_wipe = $configValuesHash{'wipe_hpa'};
	if ( defined $should_wipe &&
		 $should_wipe =~ /^[YyTt]/) {
		log_mesg("Checking for and removing HPA regions\n");
	}
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			if ($hdds[$selection]->bus eq "ATA") {
				if ( defined $should_wipe &&
					 $should_wipe =~ /^[YyTt]/) {
					my $locator = $hdds[$selection]->osLocator;
					my $hpaMaxValue = hpa_enabled_max_value($locator);
					if ($hpaMaxValue) {
						log_mesg(sprintf("HPA found, wiping HPA on ATA Drive %s\n", $locator));
						system("hdparm -Np$hpaMaxValue --yes-i-know-what-i-am-doing $locator 1>/dev/null 2>&1");
						sleep 2;
					}
					else {
						$hdds[$selection]->hpa("Doesn't Exist");
						return;
					}
					sleep 1;
					$hpaMaxValue = hpa_enabled_max_value($locator);
					if ($hpaMaxValue) {
						$hdds[$selection]->hpa("Cannot Erase");
					}
					else {
						$hdds[$selection]->hpa("Erased");
					}
				}
				else {
					$hdds[$selection]->hpa("Skipped");
				}
			}
			else {
				$hdds[$selection]->hpa("Doesn't Exist");
			}
		}
	}
}
    
#dco_exists : check if the DCO exists on this device
sub dco_exists {
	my $ret = 0;
	my $locator = $_[0];
	my @output = `hdparm -I $locator 2>&1`;
	#look through output of ATA Identify result to see if DCO feature is supported
	$ret=$?;
	if (0==$ret) {
		foreach my $line (@output) {
			if ($line =~ /Device Configuration Overlay/) {
				$ret=1;
				last;
			}
		}
	}	

	if (0 == $ret) {
		return 0;
	}
	@output = `hdparm --dco-identify $locator 2>&1`;
	$ret=$?;
	if (0==$ret) {
		foreach my $line (@output) {
			if ($line =~ /SG_IO: /) { #this indicates an error retrieving the DCO, so don't try to erase it
				return 0;
			}
			elsif ($line =~ /HDIO_DRIVE_CMD\(dco_identify\) failed: /) { #this indicates an error retrieving the DCO, so don't try to erase it
				return 0;
			}
		}
		return 1;
	}
	return 0; #error retrieving DCO information
}

#wipe_dcos : wipe DCO by resetting the DCO to factory defaults
sub wipe_dcos {
	my $should_wipe = $configValuesHash{'wipe_dco'};
	if ( defined $should_wipe &&
		 $should_wipe =~ /^[YyTt]/) {
		log_mesg("Checking for and removing DCO regions\n");
	}
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			if ($hdds[$selection]->bus eq "ATA") {
				if ( defined $should_wipe &&
					 $should_wipe =~ /^[YyTt]/) {
					my $locator = $hdds[$selection]->osLocator;
					if (dco_exists($locator)) {
						log_mesg(sprintf("Wiping DCO on ATA Drive %s\n", $hdds[$selection]->osLocator));
						system("hdparm --yes-i-know-what-i-am-doing --dco-restore ${hdds[$selection]->osLocator} 1>/dev/null 2>&1");
						$hdds[$selection]->dco("Erased");
						sleep 2;
					}
					else {
						$hdds[$selection]->dco("Doesn't Exist");
					}
				}
				else {
					$hdds[$selection]->dco("Skipped");
				}
			}
			else {
				$hdds[$selection]->dco("Doesn't Exist");
			}
		}
	}
}
#update_script : add the selected drives to erase to the template script to ensure that only those drives are erased
sub update_script {
	my @selected_hdds = @{$_[1]};
	if (! -e $_[0]) {
		log_mesg("Unable to find test script \"$_[0]\", Exiting\n",1);
		exit 1;
	}
	my $isFWErase = $_[2];
	my($filename, $dirs, $suffix) = fileparse($_[0], qr/\.[^.]*/);
	my $modified_script_name = "$tmpdir/" . $filename . "_$$" . $suffix;
	my $dom = XML::LibXML->load_xml(location => $_[0]);
	my $addedNodes=0;
	#pass 1 - update exiting Device nodes and clone them for each drive
	foreach my $testnode ($dom->findnodes('//Script/TestSet')) {
		my @devicenodes = $testnode->getElementsByLocalName("Device");
		foreach my $node (@devicenodes) {
			my $found_device_node=0;
			my $newnode = $dom->documentElement;
			for my $selection (@selected_hdds) {
				if ($selection < scalar(@hdds)) {
					if ($isFWErase && (is_drive_frozen_hdparm(\$hdds[$selection]) ||
									   !does_drive_support_fw_erase($hdds[$selection]->osLocator))) {
						next;
					}
					if ($found_device_node) {
						my $cloned_node = $newnode->cloneNode();
						$cloned_node->setAttribute( 'osLocator', $hdds[$selection]->osLocator);
						$cloned_node->setAttribute( 'hwLocator', $hdds[$selection]->hwLocator);
						$node->addSibling($cloned_node);
					}
					else {
						$node->setAttribute( 'osLocator', $hdds[$selection]->osLocator);
						$node->setAttribute( 'hwLocator', $hdds[$selection]->hwLocator);
						$newnode = $node->cloneNode();
						$found_device_node=1;
					}
					$addedNodes++;
				}
			}
		}
		#pass 2 handle testsets that have no defined device nodes by creating new ones
		if (0 == scalar(@devicenodes)) {
			for my $selection (@selected_hdds) {
				if ($selection < scalar(@hdds)) {
					if ($isFWErase && (is_drive_frozen_hdparm(\$hdds[$selection]) ||
									   !does_drive_support_fw_erase($hdds[$selection]->osLocator))) {
						next;
					}
					my $newnode = $dom->createElement("Device");
					$newnode->setAttribute( 'osLocator', $hdds[$selection]->osLocator);
					$newnode->setAttribute( 'hwLocator', $hdds[$selection]->hwLocator);
					$testnode->addChild($newnode);
					$addedNodes++;
				}
			}
		}
	}
	#original script not modified, return empty string to indicate not to run this script
	if (0 == $addedNodes) {
		return "";
	}
	open XML, ">$modified_script_name" || die "unable to write out updated XML to \"$modified_script_name\"";
	print XML $dom->toString();
	close XML;
	return $modified_script_name;
}
#run_script : run a test script through the pcd cmdline
sub run_script {
	if (! -e $_[0]) {
		log_mesg("Unable to find test script \"$_[0]\", Exiting\n",1);
		exit 1;
	}
	log_mesg("Running script : \"$_[0]\"\n");
	my($filename, $dirs, $suffix) = fileparse($_[0], qr/\.[^.]*/);
	my $diaglog_file = "$tmpdir/diaglog_${filename}.log";

	launch_pcd_cmd("./pcd run -ep in PCDARGS -f $_[0] -i $diaglog_file");
	return $diaglog_file;
}

#render_dt : render the date time in a particular format
sub render_dt {
	return $_[0]->strftime( '%Y-%m-%d %H:%M:%S%z' );
}
#render_dt_duration : render a difference between to time values (duration)
sub render_dt_duration {
	my $dur = $_[1] - $_[0];
	my @duration = $dur->in_units( 'hours', 'minutes', 'seconds' );
	for (@duration) {
		if (! defined $_) {
			$_=0;
		}
	}
	return sprintf("%02u:%02u:%02u (HH:MM:SS)", $duration[0],$duration[1],$duration[2]);
}

#get_script_run_info : parse the script run out and populate the ScriptInfo structure and child TestInfo structures
sub get_script_run_info {
	my @scriptInfoArray;
	my @diaglog_array = @{$_[0]};
	for my $diaglog (@diaglog_array) {
		if (! -e $diaglog) {
			next;
		}
		my $scriptInfo = ScriptInfo->new();
		my $dom = XML::LibXML->load_xml(location => $diaglog);
		foreach my $node ($dom->findnodes('//pcd:DiagLog/pcd:DiagInfo/pcd:Tests/pcd:Description')) {
			$scriptInfo->description($node->to_literal());
			last;
		}
		foreach my $node ($dom->findnodes('//pcd:DiagLog/pcd:DiagInfo/pcd:TestRun')) {
			my $testInfo = TestInfo->new();
			$testInfo->totalPatterns(0);
			my $startPattern = 0;
			my $endPattern = 0;
			for my $child ($node->childNodes) {
				if ($child->nodeType() != XML_ELEMENT_NODE) {next;}
				if ($child->nodeName eq "Device") {
					for my $devnodechild ($child->childNodes) {
						if ($devnodechild->nodeType() != XML_ELEMENT_NODE) {next;}
						if ($devnodechild->nodeName eq "OS_Locator") {
							$testInfo->osLocator($devnodechild->to_literal());
						}
						elsif ($devnodechild->nodeName eq "HW_Locator") {
							$testInfo->hwLocator($devnodechild->to_literal());
						}
					}
				}
				elsif ($child->nodeName eq "Test") {
					if ($child->hasAttribute("name")) {
						$testInfo->name($child->getAttribute("name"));
					}
				}
				elsif ($child->nodeName eq "TestResult") {
					$testInfo->result($child->to_literal()); 
				}
				elsif ($child->nodeName eq "Start") {
					my $value = $child->to_literal();
					$value =~ s/\s+//g;
					$testInfo->start($value);
					#2018-04-11T09:58:07.294719-07:00
					if ($value =~ /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)\.(\d+)([+-])(\d+):(\d+)/) {
						$testInfo->startDT(
							DateTime->new(
								year       => $1,
								month      => $2,
								day        => $3,
								hour       => $4,
								minute     => $5,
								second     => $6,
								nanosecond => $7 * 1000,
								time_zone  => $8 . $9 . $10,
							));
					}
				}
				elsif ($child->nodeName eq "Finished") {
					my $value = $child->to_literal();
					$value =~ s/\s+//g;
					if ($value =~ /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)\.(\d+)([+-])(\d+):(\d+)/) {
						$testInfo->endDT(
							DateTime->new(
								year       => $1,
								month      => $2,
								day        => $3,
								hour       => $4,
								minute     => $5,
								second     => $6,
								nanosecond => $7 * 1000,
								time_zone  => $8 . $9 . $10,
							));
					}
				}
				elsif ($child->nodeName eq "Parameter") {
					if ($child->hasAttribute("name")) {
						if ($child->getAttribute("name") eq "Start Pattern Index") {
							for my $devnodechild ($child->childNodes) {
								if ($devnodechild->nodeType() != XML_ELEMENT_NODE) {next;}
								if ($devnodechild->nodeName eq "Value") {
									$startPattern = $devnodechild->to_literal();
									$startPattern =~ s/\s+//g;
								}
							}
						}
						elsif ($child->getAttribute("name") eq "End Pattern Index") {
							for my $devnodechild ($child->childNodes) {
								if ($devnodechild->nodeType() != XML_ELEMENT_NODE) {next;}
								if ($devnodechild->nodeName eq "Value") {
									$endPattern = $devnodechild->to_literal();
									$endPattern =~ s/\s+//g;
								}
							}
						}
					}
				}
			}
			if ($testInfo->startDT && $testInfo->endDT) {
				$testInfo->start(render_dt($testInfo->startDT));
				$testInfo->end(render_dt($testInfo->endDT));
				$testInfo->duration(render_dt_duration($testInfo->startDT, $testInfo->endDT));			
			}
			if ($startPattern > 0 && $endPattern > 0) {
				$testInfo->totalPatterns($endPattern - $startPattern + 1);
			}
			push(@{$scriptInfo->tests}, $testInfo);
		}
		push(@scriptInfoArray, $scriptInfo);
	}
	return @scriptInfoArray;
}
#print_script_info : debugging : print ScriptInfo information to stdout
sub print_script_info {
	my @scriptInfoArray = @{$_[0]};
	foreach my $scriptInfo (@scriptInfoArray) {
		log_mesg(sprintf("Script Description: %s\n", $scriptInfo->description));
		foreach my $testInfo (@{$scriptInfo->tests}) {
			log_mesg(sprintf(" Test Info:\n"));
			log_mesg(sprintf("  Test Name: %s\n", $testInfo->name));
			log_mesg(sprintf("  Test Device HW Locator: %s\n", $testInfo->hwLocator));
			log_mesg(sprintf("  Test Device OS Locator: %s\n", $testInfo->osLocator));
			log_mesg(sprintf("  Test Start: %s\n", $testInfo->start));
			log_mesg(sprintf("  Test End: %s\n", $testInfo->end));
			log_mesg(sprintf("  Test Result: %s\n", $testInfo->result));
		}
	}
}

#didLastTestRunOnDrivePass : run through SciptInfo output in reverse order to see if last test that was run passed
sub didLastTestRunOnDrivePass {
	my $hddinfo = $_[0];
	my @scriptInfoArray = @{$_[1]};
	my $scriptLast = scalar(@scriptInfoArray) - 1;
	for (my $i=$scriptLast; $i>=0; $i--) {
		my $testLast = scalar(@{$scriptInfoArray[$i]->tests}) - 1;
		for (my $j=$testLast; $j>=0; $j--) {
			my $currScriptInfo = $scriptInfoArray[$i];
			my $currTestInfo = @{$currScriptInfo->tests}[$j];
			if ($currTestInfo->osLocator eq $hddinfo->osLocator &&
				$currTestInfo->hwLocator eq $hddinfo->hwLocator) {
				if ($currTestInfo->result eq "Skipped") {
					next;
				}
				if (($currTestInfo->result eq "Completed"
					 || $currTestInfo->result eq "Passed")
					&& (0 == write_or_verify_fw_erase_patterns($hddinfo->osLocator, 1))
					) {
					$hddinfo->fweraserounds($hddinfo->fweraserounds + 1);
					return 1;
				}
				else {
					return 0;
				}
			}
		}
	}
	return 0; #no test run on device
}

#store_drive_signatures : store drive signatures away, in case wiping the HPA and DCO corrupts these values
sub store_drive_signatures {
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			my $osLocator = $hdds[$selection]->osLocator;
			$osLocator =~ s/\//_/g;
			my $outfile = "$tmpdir/" . $osLocator . "_$$.bin";
			system("dd if=${hdds[$selection]->osLocator} of=$outfile bs=512 count=2 1>/dev/null 2>&1");
		}
	}
}

my $randSeed=0; #keep track of this so we can recreate random distribution

#write_or_verify_pattern : write or verify a consectuive set of sectors with a particular input pattern that is 1 byte long
#for conformance to NIST Purge Verification standard
sub write_or_verify_pattern {
	my $osLocator = $_[0];
	my $verify = $_[1];
	my $preerasefile = $_[2];
	my $posterasefile = $_[3];
	my $offset = $_[4];
	my $count = $_[5];
	my $blkSize = $_[6];
	my $preerasefiletmp = sprintf("$tmpdir/erase_verify_data_preerasetmp_%s_1.bin", $$);

	#create a larger file with the pattern repeated many times to speed up the later compare routines
	my $optimizedWriteAmount = 4096;
	if ( ! -e $preerasefiletmp) {
		log_mesg("Initializing Firmware Verification...\n");
		my $ret = 0;
		for (my $idx=0; $idx<$optimizedWriteAmount || $ret; $idx++) {
			system("dd if=$preerasefile of=$preerasefiletmp bs=$blkSize count=1 seek=$idx 1>/dev/null 2>&1");
		}
		if ($ret) {
			log_mesg("Error: dd failed to write pattern, Exiting\n",1);
			return 1;
		}
	}
	if ($verify) {
		if ($count < $optimizedWriteAmount) {
			$optimizedWriteAmount = $count;
		}
		my $currOffset = $offset;
		for (my $idx=0; $idx<$count; $idx+=$optimizedWriteAmount) {
			if ($idx + $optimizedWriteAmount > $count) {
				$optimizedWriteAmount = $count - $idx;
			}
			my $offsetInBytes = $currOffset * $blkSize;
			my $cmpInBytes = $optimizedWriteAmount * $blkSize;
			my $ret = system("cmp $osLocator $preerasefiletmp --bytes=$cmpInBytes --ignore-initial=$offsetInBytes:0 1>/dev/null 2>&1");
			#log_mesg("cmp $osLocator $preerasefiletmp --bytes=$cmpInBytes --ignore-initial=$offsetInBytes:0\n",0,0);
			if (0 == $ret) {
				log_mesg("Error: cmp found bin files to match\n");
				return 1;
			}
			$currOffset += $optimizedWriteAmount
		}
	}
	else {
		my $currOffset = $offset;
		for (my $idx=0; $idx<$count; $idx+=$optimizedWriteAmount) {
			if ($idx + $optimizedWriteAmount > $count) {
				$optimizedWriteAmount = $count - $idx;
			}
			my $ret = system("dd if=$preerasefiletmp of=$osLocator bs=$blkSize count=$optimizedWriteAmount seek=$currOffset 1>/dev/null 2>&1");
			#log_mesg("dd if=$preerasefiletmp of=$osLocator bs=$blkSize count=$optimizedWriteAmount seek=$currOffset\n",0,0);
			if ($ret) {
				log_mesg("Error: dd failed to write pattern, Exiting\n",1);
				return 1;
			}
			$currOffset += $optimizedWriteAmount;
		}
	}
	return 0; #zero means success
}

#write_or_verify_fw_erase_patterns : go through the entire drive and write or verify a percentage of sectors (set to 10%) at random offsets within
# a fixed number of segments (1000)
#for conformance to NIST Purge Verification standard
sub write_or_verify_fw_erase_patterns {
	my $osLocator = $_[0];
	my $verify = $_[1];
	my $stage = $verify+1;
	
	my $numSectors = 0;
	my $blkCnt512= 0;
	my $blkSize = 0;
	find_drive_block_count($_[0], \$numSectors, \$blkCnt512, \$blkSize);
	
	if ($blkSize < 512) {
		return 1; #error
	}
	if (0 == $numSectors) {
		return 0; #nothing to verify
	}
	
	my $verify_file_preerase = sprintf("$tmpdir/erase_verify_data_preerase_%s_1.bin", $$);
	my $verify_file_posterase = sprintf("$tmpdir/erase_verify_data_posterase_%s_1.bin", $$);

	if ( ! $verify ) {
		if ( ! -e $verify_file_preerase ) { #create this once
			my $ret = system("dd if=/dev/urandom of=$verify_file_preerase bs=$blkSize count=1 1>/dev/null 2>&1");
			if ($ret) {
				log_mesg("dd failed to write pattern, Exiting\n",1);
				return 1;
			}
		}
	}
	
	my $totalSections = 1000;
	
	my $sectorsToVerify = int($numSectors * ($drivePercentage/100.0));
	if (0 == $sectorsToVerify) {
		return 0; #nothing to verify
	}
	my $jumpAmount = int($numSectors/$totalSections);
	#log_mesg("total sectors = $sectorsToVerify\n");

	if ($sectorsToVerify < $totalSections) {
		$sectorsToVerify = $totalSections;
	}
	
	my $writeTotalAmount = int($sectorsToVerify / $totalSections);

	if ($writeTotalAmount < 2) {
		$writeTotalAmount=2; #make sure there are 2 writes per section at a minimum
	}
	my $writeAmount = int($writeTotalAmount / 2);

	my $segmentDivisor = int($drivePercentage);
	if ($segmentDivisor < 1) {
		$segmentDivisor=1;
	}
	my $totalSegments = int(100.0/$segmentDivisor);
	my $lastLBA = $numSectors-1;

	#verify first sector
	my $ret = write_or_verify_pattern($osLocator, $verify, $verify_file_preerase, $verify_file_posterase, 0, 1, $blkSize);
	if ($ret) {return $ret;}
	
	#verify last sector
	$ret = write_or_verify_pattern($osLocator, $verify, $verify_file_preerase, $verify_file_posterase, $lastLBA, 1, $blkSize);
	if ($ret) {return $ret;}
	
	#generate rand seed only once
	if (!$verify) {
		$randSeed=rand();
	}
	#reseed so number set is reproducible
	srand($randSeed);
	tie my $timer, 'Time::Stopwatch';
	my $percentDone=0;
	log_mesg(sprintf("Firmware Erase Verification ($osLocator) at $drivePercentage%% - Stage $stage %d%% complete\n", 0));
	for (my $lba=0; $lba<$numSectors; $lba+=$jumpAmount) {
		if ($timer > 5) {
			my $currPercentDone=int(($lba/$numSectors)*100.0);
			if ($currPercentDone > $percentDone && $currPercentDone < 100) {
				log_mesg(sprintf("Firmware Erase Verification ($osLocator) at $drivePercentage%% - Stage $stage %d%% complete\n", $currPercentDone));
				$percentDone=$currPercentDone;
			}
			$timer=0;
		}
		my $writeAmountTmp = $writeAmount;
		my $segment1 = $lba;
		my $writeSegementIdx1 = 0;
		if ($lba + $jumpAmount > $numSectors) { #end of drive
			my $totalSegmentSize = $lastLBA-$lba;
			if ($totalSegmentSize < $writeTotalAmount) {
				$writeAmountTmp = $totalSegmentSize;
				$totalSegments = 1;
			}
			else {
				$totalSegments = int($totalSegmentSize / $writeAmount);
			}
		}
		$writeSegementIdx1 = int(rand($totalSegments));
		$segment1 = $lba + ($writeAmountTmp * $writeSegementIdx1);
		$ret = write_or_verify_pattern($osLocator, $verify, $verify_file_preerase, $verify_file_posterase, $segment1, $writeAmountTmp, $blkSize);
		if ($ret) {return $ret;} #error : error msg printed by subroutine

		#NIST Purge verify : choose a second random segment to verify
		if ($totalSegments > 1) {
			#NIST Purge verify : ensure the segements don't overlap
			my $writeSegementIdx2 = int(rand($totalSegments));
			while ($writeSegementIdx2 == $writeSegementIdx1) {
				$writeSegementIdx2 = int(rand($totalSegments));
			}
			my $segment2 = $lba + ($writeAmountTmp * $writeSegementIdx2);
			$ret = write_or_verify_pattern($osLocator, $verify, $verify_file_preerase, $verify_file_posterase, $segment2, $writeAmountTmp, $blkSize);
			if ($ret) {return $ret;} #error : error msg printed by subroutine
		}
		#else : we are either verifying 100% of the drive or at the end of drive with very little sectors left to test
	}
	log_mesg(sprintf("Firmware Erase Verification ($osLocator) at $drivePercentage%% - Stage $stage %d%% complete\n", 100));
	return 0; #zero means success
}

#does_drive_support_fw_erase : use the test list hash populated earlier
#to determine if this drive support FW erase so we can avoid writing the verification patterns
#if it does not
sub does_drive_support_fw_erase {
	my @fwTestList = ( 
		"MMCSecureEraseTest",
		"SCSISanitizeBlockEraseTest",
		"SCSISanitizeCryptoEraseTest",
		"SCSISanitizeOverwriteTest",
		"NVMeAdvancedSecureEraseTest",
		"NVMeSecureEraseTest",
		"ATAEnhancedSecureEraseTest",
		"ATASecureEraseTest"
		);
	if (exists $testListHash{$_[0]}) {
		my @array = @{$testListHash{$_[0]}};
		for my $testKey (@array) {
			for my $fwTestKey (@fwTestList) {
				if ($testKey eq $fwTestKey) {
					return 1;
				}
			}
		}
		return 0;
	}
	return 0;
}

#write_patterns_to_selected_drives : write pattern for NIST Purge verification to each drive selected for erasure
sub write_patterns_to_selected_drives {
	log_mesg("Writing Patterns for Firmware Verification...\n");
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			if (0 == does_drive_support_fw_erase($hdds[$selection]->osLocator)) {
				log_mesg(sprintf("Drive does not support FW erase %s, not writing verification patterns...\n", $hdds[$selection]->osLocator),1);
				$hdds[$selection]->verifyPatterns(0);
			}
			elsif (is_drive_frozen_hdparm(\$hdds[$selection])) {
				log_mesg(sprintf("Drive is frozen %s, not writing verification patterns...\n", $hdds[$selection]->osLocator),1);
				$hdds[$selection]->verifyPatterns(0);
			}
			elsif (write_or_verify_fw_erase_patterns($hdds[$selection]->osLocator, 0)) {
				log_mesg(sprintf("Unable to write verify patterns to %s...\n", $hdds[$selection]->osLocator),1);
				$hdds[$selection]->verifyPatterns(0);
			}
		}
	}
}

#getSecondPassDrives : determine is the first pass (FW erase) was not successful to determine if the 2nd pass is necessary (Block Erase/Drive Wipe)
#option --forceblkerase overrides this and causes all drives to run 2nd pass erasure
sub getSecondPassDrives {
	my @scriptInfoArray = @{$_[0]};
	my @secondPassDrives = ();
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			if (0 == $hdds[$selection]->verifyPatterns) {
				log_mesg(sprintf("Unable to verify patterns on Drive %s...\n", $hdds[$selection]->osLocator),1);
				push(@secondPassDrives, $selection);
			}
			elsif (0 == didLastTestRunOnDrivePass($hdds[$selection], \@scriptInfoArray)) {
				log_mesg(sprintf("Drive %s did not successfully run FW erase, running Drive Wipe...\n", $hdds[$selection]->osLocator),1);
				push(@secondPassDrives, $selection);
			}
			elsif ($forceblkerase) { #run second pass anyway
				push(@secondPassDrives, $selection);
			}
		}
	}
	return @secondPassDrives;
}

#addCustomFields : find custom field comment tag in template HTML and reaplce with real values
sub addCustomFields {
	my $outValue="";
	for my $customFieldsIndex (@customFieldsIndices) {
		if (exists($customFieldsHash{$customFieldsIndex})) {
			my $customField = $customFieldsHash{$customFieldsIndex};
			if (length($customField->name)) {
				$outValue .= "<div class=\"label\">" . encode_entities($customField->name) . ":</div>\n";
				my $value = "<!-- CUSTOM_VALUE(@{[$customField->name]}) -->" . encode_entities($customField->value);
				$outValue .= "<div class=\"data fullrow\">" . $value . "</div>\n";
			}
		}
	}
	if (length($outValue)) {
		$outValue = $outValue . "<div class=\"chapter\">\n";
		$outValue .= "</div>\n";
	}
	return $outValue;
}


#replaceTokenHtml : find a particular token in the HTML tempalte and replace with the real value
sub replaceTokenHtml {
	my $inStr = $_[0];
	my $token = $_[1];
	my $outStr="";
	if (defined $_[2]) {
		my $replacement = encode_entities($_[2]);
		$outStr = $inStr;
		$outStr =~ s/\$$token/$replacement/g;
	}
	return $outStr;
}
#write_hdd_html : generate the HTML report for a particular drive that was erased
sub write_hdd_html {
	my $retCode=0;
	my $inFh = $_[0];
	my $outFh = $_[1];
	my $hddinfo = $_[2];
	my @scriptInfoArray = @{$_[3]};
	my $reportUUID = `uuidgen`;
	$reportUUID =~ s/\s+//g;
	my $digitalSignature = `openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:512 2>/dev/null | sed -n '2p'`;
	$digitalSignature =~ s/\s+//g;
	my $date = `date +"%Y-%m-%d %H:%M:%S %z"`;
	$date =~ s/^\s+|\s+$//g;
	#$date =~ s/\s+//g;

	#set defaults if no tests were ever launched
	my $scriptInfo = ScriptInfo->new(
		description => '-',
		tests => ()
		);
	my $eraseInfo = TestInfo->new(
		name => '-',
		startDT => '-',
		endDT => '-',
		start => '-',
		end => '-',
		duration => '-',
		result => 'Test Not Started',
		osLocator => '-',
		hwLocator => '-',
		totalPatterns => 0
		);

	my $scriptLast = scalar(@scriptInfoArray) - 1;
	for (my $i=$scriptLast; $i>=0 && 0==scalar(@{$scriptInfo->tests}); $i--) {
		my $testLast = scalar(@{$scriptInfoArray[$i]->tests}) - 1;
		for (my $j=$testLast; $j>=0; $j--) {
			my $currScriptInfo = $scriptInfoArray[$i];
			my $currTestInfo = @{$currScriptInfo->tests}[$j];
			if ($currTestInfo->osLocator eq $hddinfo->osLocator &&
				$currTestInfo->hwLocator eq $hddinfo->hwLocator) {
				if ($currTestInfo->result eq "Skipped") {
					next;
				}
				$scriptInfo=$currScriptInfo;
				$eraseInfo=$currTestInfo;
				last;
			}
		}
	}
	my $eraseResultColor="red";
	if ($eraseInfo->result eq "Completed" ||
		$eraseInfo->result eq "Passed") {
		$eraseInfo->result("Erased"); #change test result string to match customer expectations
		$eraseResultColor = "green";
	}
	else {
		$retCode = 1;
	}
	my $eraseResultHTML=sprintf("<span class=\"highlight\" style=\"color: %s;\">", $eraseResultColor) . $eraseInfo->result . "</span>";

	#print "Size of test array = " . scalar(@{$scriptInfo->tests}) . "\n";
	#print "Size of script array = " . scalar(@scriptInfoArray) . "\n";

	my $ret = system("./$driveEraseSignatureName @{[$hddinfo->osLocator]} @{[$hddinfo->osLocator]} \"$digitalSignature\" \"@{[$scriptInfo->description]} \(@{[$eraseInfo->name]}\)\""); #write the fingerprint value
	if ($ret) {
		my $osLocator = $hddinfo->osLocator;
		$osLocator =~ s/\//_/g;
		my $sigfile = "$tmpdir/" . $hddinfo->osLocator . "_$$.bin";
		if (-e $sigfile) {
			$ret = system("./$driveEraseSignatureName $sigfile @{[$hddinfo->osLocator]} \"$digitalSignature\" \"@{[$scriptInfo->description]} \(@{[$eraseInfo->name]}\)\""); #write the fingerprint value
		}
	}
	if ($ret) {
		$retCode = 1;
	}
	while(<$inFh>) {
		if ($_ =~ /CustomFields/) {
			$_=addCustomFields();
		}
		else {
			$_ = replaceTokenHtml($_, "DiskNum", $hddinfo->idx);
			$_ = replaceTokenHtml($_, "HddVendor", $hddinfo->vendor);
			$_ = replaceTokenHtml($_, "HddSize", $hddinfo->marketedSize);
			$_ = replaceTokenHtml($_, "HddHPA", $hddinfo->hpa);
			$_ = replaceTokenHtml($_, "HddModel", $hddinfo->model);
			$_ = replaceTokenHtml($_, "HDDBus", $hddinfo->bus);
			$_ = replaceTokenHtml($_, "HddDCO", $hddinfo->dco);
			$_ = replaceTokenHtml($_, "HddSerial", $hddinfo->serial);
			$_ = replaceTokenHtml($_, "HddLogicalSectorCount", $hddinfo->sectorCount);
			$_ = replaceTokenHtml($_, "HddRemappedSectorsPostErase", $hddinfo->remappedSectorsPostErase);
			$_ = replaceTokenHtml($_, "HddRemappedSectors", $hddinfo->remappedSectors);
			$_ = replaceTokenHtml($_, "HddHealth", $hddinfo->health);
			
			$_ = replaceTokenHtml($_, "ErasureStartTime", $eraseInfo->start);
			$_ = replaceTokenHtml($_, "ErasureEndTime", $eraseInfo->end);
			$_ = replaceTokenHtml($_, "ErasureDuration", $eraseInfo->duration);
			$_ = replaceTokenHtml($_, "ErasureMethod", "@{[$scriptInfo->description]} \(@{[$eraseInfo->name]}\)");
			my $totalEraseRounds = $hddinfo->fweraserounds + $eraseInfo->totalPatterns;
			my $eraseStrVal = $totalEraseRounds;
			if ($eraseInfo->totalPatterns || $hddinfo->fweraserounds) {
				$eraseStrVal .= " (";
				if ($eraseInfo->totalPatterns) {
					$eraseStrVal .= sprintf("%d overwriting", $eraseInfo->totalPatterns);
				}
				if ($hddinfo->fweraserounds) {
					if ($eraseInfo->totalPatterns) {$eraseStrVal .= ", "};
					$eraseStrVal .= sprintf("%d firmware", $hddinfo->fweraserounds);
				}
				$eraseStrVal .= ")";
			}
			$_ = replaceTokenHtml($_, "ErasureRounds", $eraseStrVal);
			$_ =~ s/\$ErasureResult/$eraseResultHTML/g;
			
			my $verificationMethod="Quick Sampling";
			#a greater than zero total patterns implies that the drive wipe
			#test was run with a full verification of the overwrite
			if ($eraseInfo->totalPatterns) {
				$verificationMethod="Full";
			}
			$_ = replaceTokenHtml($_, "VerificationMethod", $verificationMethod);
			
			$_ = replaceTokenHtml($_, "SysManufacturer", $sysInfo->vendor);
			$_ = replaceTokenHtml($_, "SYSMANUFACTURER", $sysInfo->vendor);
			$_ = replaceTokenHtml($_, "SysChassisType", $sysInfo->chassis);
			$_ = replaceTokenHtml($_, "SysProduct", $sysInfo->product);
			$_ = replaceTokenHtml($_, "SYSPRODUCT", $sysInfo->product);
			$_ = replaceTokenHtml($_, "SysSerialNumber", $sysInfo->serial);
			$_ = replaceTokenHtml($_, "SYSSERIALNUMBER", $sysInfo->serial);
			$_ = replaceTokenHtml($_, "SysUUID", $sysInfo->uuid);
			$_ = replaceTokenHtml($_, "ReportUUID", $reportUUID);
			$_ = replaceTokenHtml($_, "ReportDate", $date);
			$_ = replaceTokenHtml($_, "REPORTDATE", $date);
			$_ = replaceTokenHtml($_, "SoftwareName", $appInfo->name);
			$_ = replaceTokenHtml($_, "SOFTWARENAME", $appInfo->name);
			$_ = replaceTokenHtml($_, "SoftwareVersion", $appInfo->version);
			$_ = replaceTokenHtml($_, "SOFTWAREVERSION", $appInfo->version);
			$_ = replaceTokenHtml($_, "HddDigitalSignature", $digitalSignature);

			#custom sysinfo fields
			if ($_ =~ /CustomSysInfo/) {
				$_ = addCustomSysinfo();
			}
		}
		print $outFh $_;
	}
	return $retCode;
}

#create_file_name_from_hdd : use information from drive to create a unque filename along with the current PID (for uniqueness)
sub create_file_name_from_hdd {
	my $hdd = shift;
	my $model = $hdd->model;
	my($filename, $dirs, $suffix) = fileparse($hdd->osLocator);
	my $name = $filename;
	if (length($model)) {
		$model =~ s/ /-/g; #replace all spaces the model name with dashes
		$model =~ s/[^A-Za-z0-9\-\.]//g; #sanitize the model name
		$name = $name . "_" . $model;
	}
	my $date = `date +"%Y-%m-%dT%H-%M-%S"`;
	$date =~ s/^\s+|\s+$//g;
	$name = $date . "_" . $name . "_" . "$$";
}

#print_html_reports_per_device :  generate the HTML report for a all drives that were erased
sub print_html_reports_per_device {
	my $retCode=0;
	my @scriptInfoArray = @{$_[0]};
	my $counter=0;
	for my $selection (@selected_drives) {
		if ($selection < scalar(@hdds)) {
			$hdds[$selection]->remappedSectorsPostErase(find_remapped_sectors($hdds[$selection]->osLocator));
			$counter++;
			my $unique_file_name = create_file_name_from_hdd($hdds[$selection]);
			my $filename = sprintf("$tmpdir/%s.html", $unique_file_name);
			#moved to separate menu item
			#my $filenamePdf = sprintf("$outdir/%s.pdf", $unique_file_name);
			open(my $fh, ">", "$filename")
				or die "Can't open > $filename: $!";
			open(my $inHtmlFd, "<", "$template_html")
				or die "Can't open < $template_html: $!";
			my $ret = write_hdd_html($inHtmlFd,$fh,$hdds[$selection], \@scriptInfoArray);
			if ($ret) {
				$retCode = $ret;
			}
			close($fh);
			close($inHtmlFd);
		}
	}
	return $retCode;
}


#get_report_names : collect the PDF filenames in output directory and return them in an array
sub get_report_names {
	if (opendir(DIR, $_[0])) {
		my @dir_contents = readdir DIR;
		my @sorted_dir_contents = sort @dir_contents;
		closedir DIR;
		@{$_[1]} = ();
		for my $file (@sorted_dir_contents) {
			if ($file =~ /\.pdf$/) {
				push(@{$_[1]}, $file);
			}
		}
	}
}

#get_report_names_html : collect the HTML filenames in tmp directory and return them in an array
#explicitly disregard the report template
sub get_report_names_html {
	if (opendir(DIR, $_[0])) {
		my @dir_contents = readdir DIR;
		my @sorted_dir_contents = sort @dir_contents;
		closedir DIR;
		@{$_[1]} = ();
		for my $file (@sorted_dir_contents) {
			if ($file =~ /\.html$/ &&
				$file !~ /^report_template/) {
				push(@{$_[1]}, $file);
			}
		}
	}
}

#preview_reports : preview the HTML reports by temporarily transforming them to PDFs and viewing those with xpdf
sub preview_reports {
	my $reports_dir = $_[0];
	my @report_array = ();
	my @menu_opts = ('--cancel-label', 'Exit', '--menu');
	get_report_names_html($reports_dir, \@report_array);
	my $menu_title = "Choose Erasure Report to preview from list :";
	my $loop = 1;
	while ( $loop ) {
		my $ret = show_menu2(\@report_array, \@menu_opts, $menu_title, 'REPORTS');
		if ($ret =~ /[0-9]+/) {
			if ($ret > 0) {
				$ret--;
			}
			if ($ret < scalar(@report_array)) {
				my $report_name = "$reports_dir/$report_array[$ret]";
				my($filename, $dirs, $suffix) = fileparse($report_name, qr/\.[^.]*/);
				my $filenamePdf = sprintf("$reports_dir/%s_preview.pdf", $filename);
				if (can_run('wkhtmltopdf')) {
					log_mesg("Generating preview report: $filenamePdf\n");
					system("wkhtmltopdf $report_name $filenamePdf 1>/dev/null 2>&1");
				}
				if (-e $filenamePdf) {
					my @args = ('sh', '-c', "xpdf $filenamePdf", '&>', '>/dev/null');
					system(@args);
				}
				else {
					msgbox('Cannot Open', "Cannot open report file: $filenamePdf");
				}
			}
		}
		elsif ($ret eq 'c') {
			$loop=0;
		}
	}
}
#view_reports : view the output PDF reports with xpdf
sub view_reports {
	my $reports_dir = $_[0];
	my @report_array = ();
	my @menu_opts = ('--cancel-label', 'Exit', '--menu');
	get_report_names($reports_dir, \@report_array);
	my $menu_title = "Choose Erasure Report to view from list :";
	my $loop = 1;
	while ( $loop ) {
		my $ret = show_menu2(\@report_array, \@menu_opts, $menu_title, 'REPORTS');
		if ($ret =~ /[0-9]+/) {
			if ($ret > 0) {
				$ret--;
			}
			if ($ret < scalar(@report_array)) {
				my $report_name = "$reports_dir/$report_array[$ret]";
				if (-e $report_name) {
					my @args = ('sh', '-c', "xpdf $report_name", '&>', '>/dev/null');
					system(@args);
				}
				else {
					msgbox('Cannot Open', "Cannot open report file: $report_name");
				}
			}
		}
		elsif ($ret eq 'c') {
			$loop=0;
		}
	}
}

#is_license_server_failure : Determine if the pcd client failed with a server license auth error
#or some other license error. If it is license server error, we use this to prompt for a new server
#IP. Otherwise launch_pcd_cmd will simply exit with an error
sub is_license_server_failure {
	my $ret = system("grep -qs \"SERVER_CHECK\" $_[0]");
	if ($ret) {
		$ret = system("grep -qs \"You must specify --server\" $_[0]");
	}
	return $ret;
}

#launch_pcd_cmd : launch the PCD cmdline client
#try to handle authentication errors by prompting user for IP in a loop
sub launch_pcd_cmd {
	my $ret = LICENSEAUTHENTICATIONFAILED;
	my $ipaddr = $remote_server_ip;
	my $mesg="Authentication Error";
	my $pcd_tmp_file = "$tmpdir/pcd_tmp_file.txt";
	my $cmd = "bash -c \"$_[0]\"";
	if (defined $_[1]) {
		$pcd_tmp_file = $_[1];
	}
	else {
		$cmd = "bash -c \"$_[0] | tee $pcd_tmp_file; exit \\\${PIPESTATUS[0]}\"";
	}
	while (LICENSEAUTHENTICATIONFAILED == $ret) {
		my $cmdToRun = $cmd;
		if (length($ipaddr)) {
			$cmdToRun =~ s/PCDARGS/-s \Q$ipaddr\E/g;
		}
		else {
			$cmdToRun =~ s/PCDARGS//g;
		}
		system($cmdToRun);
		$ret = $?;
		if ($ret) {
			$ret = $ret >> 8;
			if (LICENSEAUTHENTICATIONFAILED == $ret) {
				my $ret = 1; #default is an error return unless user can provide an IP address of a valid server
				if (0==$autoerase && !is_license_server_failure($pcd_tmp_file)) {
					open(CPERR, ">&STDERR");
					open(STDERR, ">$tmpdir/inputip.$$");
					system("dialog --cancel-label Cancel --inputbox '${mesg}. Enter License Server IP Address' 0 0 $ipaddr");
					$ret = $? >> 8;
				}
				unless ($ret) {
					my $FILE;
					if (open(FILE, "$tmpdir/inputip.$$")) {
						$ipaddr = join("", <FILE>);
						close(FILE);
					}
					close(STDERR);
					open(STDERR, ">&CPERR");
					$ipaddr =~ s/^\s+|\s+$//g;
					if (0 == length($ipaddr)
						|| $ipaddr =~ / /) {
						$mesg="Invalid Address Format";
						$ipaddr = "";
					}
					else {
						$mesg="Authentication Error";
					}
				}
				else {
					log_mesg("License Authentication error, Exiting\n",1);
					exit 1;
				}
			}
			else {
				last;
			}
		}
	}
	$remote_server_ip = $ipaddr; #set the new ip addr if it changed
}

#assign_drive_numbers : assign drive numbers to be used in report, starting at 1
sub assign_drive_numbers {
	for (my $idx=0; $idx<scalar(@hdds); $idx++) {
		$hdds[$idx]->idx($idx+1);
		$hdd_names[$idx] = ($idx+1) . $hdd_names[$idx];
	}
}

#create_test_list : parse PC-Doctor test list, associate OS Locator with tests for use later
#in determining if the FW erase is supported
sub create_test_list {
	my @tests = ();
	my $osLocator = "";
	my $fh;
	if (!open($fh, "<", "$testlist_tmp_file")) {
		msgbox('Cannot Open', "Cannot open testlist file: $testlist_tmp_file");
		return 0;
	}
	my $currentLocator="";
	while(<$fh>) {
		if ($_ =~ /OS Locator:\s+(.*)/) {
		    if (length($currentLocator) && scalar(@tests)) {
				my @currentArray = @tests;
				$testListHash{$currentLocator} = \@currentArray;
				@tests = ();
		    }
			
			$currentLocator = trimWhiteSpace($1);
		}
		elsif ($_ =~ /\s*\d:\s*Test:\s*(\S+) \(/) {
			push(@tests, $1);
		}
	}
	if (length($currentLocator) && scalar(@tests)) {
		$testListHash{$currentLocator} = \@tests;
	}

	close($fh);
}

#wait_for_network : use "ip" command to try to wait for network to come back up
#to try to ensure that networking is in a good state before continuing
#only wait for a certain number of seconds maximum before timing out
sub wait_for_network {
	log_mesg("Waiting for network...");
	my $networkManager = "/usr/sbin/wicd";
	my $outValue = "";
	my $ctr=0;
	my $totalRetries = 20;
	my $halfRetries = int($totalRetries/2);
	do {
		if ((-e $networkManager)  && $ctr == $halfRetries) {
			log_mesg("\nRestarting network daemon...\n");
			system("systemctl restart wicd");
			log_mesg("Waiting for network...");
		}
		sleep(2);
		my @output = `ip route`;
		$outValue = join('', @output);
		$outValue =~ s/\s//g;
		$ctr++;
	}
	while (0 == length($outValue) && $ctr < $totalRetries);
	if (0 == length($outValue)) {
		log_mesg("timeout\n");
	}
	else {
		log_mesg("done\n");
	}
}

#do_drive_erase : erase drives with a 2-pass algorithm
#first pass : FW erase
#second pass : block erase
sub do_drive_erase {
	#this can be called mutliple times, so
	#reset these data structures
	#as we are erasing drives again
	@hdds = ();
	@hdd_names = ();
	@selected_drives = ();
	%testListHash = ();
	
	import_drive_wipe_overrides();
	
	log_mesg("Enumerating Drives...\n");
	
	launch_pcd_cmd("./pcd sysinfo -ep in PCDARGS -i $sysinfo_tmp_file 1>$pcd_output_file 2>&1", $pcd_output_file);

	if (! -e $sysinfo_tmp_file) {
		log_mesg("Unable to create system information file \"$sysinfo_tmp_file\", Exiting\n",1);
		exit 1;
	}

	my $dom = XML::LibXML->load_xml(location => $sysinfo_tmp_file);

	$appInfo->name("PC-Doctor Factory for Linux");
	foreach my $node ($dom->findnodes('//pcd:DiagLog/pcd:Application/pcd:version')) {
		$appInfo->version($node->to_literal());
	}
	foreach my $node ($dom->findnodes('//pcd:DiagLog/pcd:SysInfo/pcd:Device')) {
		process_node($node);
	}

	if (0 == scalar(@hdds)) {
		log_mesg("No Hard Drives enumerated, Exiting\n",1);
		exit 1;
	}
	assign_drive_numbers();

	if (0 == $autoerase) {
		my @menu_opts = ('--separate-output', '--checklist');
		while (0 == scalar(@selected_drives)) {
			@selected_drives = show_menu(\@hdd_names, \@menu_opts, '"Choose the drives to erase:"', 'DEVICES');
		}
	}
	else {
		@selected_drives = ( '0' ); #all drives selected if prompting is off
	}

	my $all_drives_selected=0;
	for my $selection (@selected_drives) {
		if ($selection eq "c") {
			log_mesg("Erase Cancelled, exiting\n");
			return;
		}
		if (looks_like_number($selection)) {
			if (0 == $selection) {
				$all_drives_selected=1;
			}
			elsif ($selection > 0) {
				$selection = $selection - 1;
			}
		}
	}
	if ($all_drives_selected) {
		@selected_drives = ();
		for (my $cntr=0; $cntr < scalar(@hdds); $cntr++) {
			push(@selected_drives, $cntr);
		}
	}
	
	wipe_hpas();
	wipe_dcos();
	
	my $frozen_drives_selected = search_for_frozen_drives();
	my $inaccessible_hpas_selected = search_for_inaccessible_hpas(0);

	
	if ($frozen_drives_selected || $inaccessible_hpas_selected) {		
		my $attempt_unfreeze_value = $configValuesHash{'attempt_unfreeze'};
		if ( defined $attempt_unfreeze_value &&
			 $attempt_unfreeze_value =~ /^[NnFf]/) {
			log_mesg("User selected to not attempt unfreeze\n");
		}
		else {		
			my $ret = 1;
			if (0 == $autoerase) {
				$ret = msgbox_yes_no_cancel("Frozen Drives or Inaccessible HPAs Detected", "***WARNING: you are trying to wipe at least one ATA Frozen drive or an inaccessible HPA region. This program can attempt to put the system to sleep in an attempt to unfreeze the drive(s) and/or access the HPA region(s). This may or may not work depending on your BIOS/drive(s). If a drive is plugged in through an external dock, you may instead try to power cycle the external dock, this may also free up the drive. WARNING: depending on Linux ACPI support for your system, this may result in an unresponsive system that may need to be power cycled. ******If the system does not wakeup after 5 seconds, you will need to wake it up by pressing the power button.****** You may opt not to put the system to sleep, in which case the drive(s) in question will be block erased, but not firmware erased. For the inaccessible HPA region, they will *otherwise* not be accessible until a power cycle is performed. Try to put system to sleep?");
			}
			if ($autoerase || (0 == $ret)) {
				log_mesg("Attempting to put system to sleep\n");
				$ret = system("rtcwake -m mem -s 10");
				if ($ret) {
					log_mesg("*** WARNING: rtcwake failed, trying to manually put system to sleep, you may need to wake it up by pressing the power button if it doesn not wake up automatically in 5 seconds.\n\n");
					sleep(15);
					system("echo +10 > /sys/class/rtc/rtc0/wakealarm 2>/dev/null; echo -n mem > /sys/power/state");
				}
				sleep(5);
				wait_for_network();
				$frozen_drives_selected=search_for_frozen_drives();
				$inaccessible_hpas_selected = search_for_inaccessible_hpas(1);
				if ($frozen_drives_selected || $inaccessible_hpas_selected) {
					if (0==$autoerase) {
						$ret = msgbox_ok_cancel('Drives Still Frozen or Inaccessible HPAs Still Detected', "*** WARNING: some drives are still frozen or HPA not accessible, do you wish to continue anyway?", 'Yes', 'Cancel');
						if ($ret > 0) {
							log_mesg("*** canceling wipe\n",1);
						return;
						}
					}
					else {
						log_mesg("*** WARNING: ",1);
						if ($frozen_drives_selected) {
							log_mesg("some drives are still frozen, ",1);
						}
						if ($inaccessible_hpas_selected) {
							log_mesg("some HPAs are inaccessible, ",1);
						}
						log_mesg("proceeding with wipe\n",1);
					}
				}
			}
			elsif (3 == $ret) {
				log_mesg("continuing without attempting to put system to sleep\n");
			}
			else {
				log_mesg("*** canceling wipe\n",1);
				return;
			}
		}
	}

	#now, check for mounted drives, try to unmount
	$ret = unmount_drives();
	if (0 == $ret) {
		log_mesg("Unable to mount at least one drive. Ensure all drives to be erased are unmounted and try again, exiting\n",1);
		exit(1);
	}


	#create PC-Doctor Test Script and perform the wipe procedures in parallel

	launch_pcd_cmd("./pcd list -ep in PCDARGS 1>$testlist_tmp_file 2>&1", $testlist_tmp_file);

	if (! -e $testlist_tmp_file) {
		log_mesg("Unable to create test list file \"$testlist_tmp_file\", Exiting\n",1);
		exit 1;
	}
	create_test_list();

	my @drivesForSecondPass = @selected_drives;
	my @scriptInfoArray = ();
	my $modified_script_name = update_script($fw_erase_script, \@selected_drives, 1);
	if (length($modified_script_name)) {
		if (! -e $modified_script_name) {
			log_mesg("Unable to create updated test script \"$modified_script_name\", Exiting\n",1);
			exit 1;
		}

		#fw verification : write to drives at known locations and verify that patterns are not present after wipe
		write_patterns_to_selected_drives();
		
		my $diaglog1 = run_script($modified_script_name);

		my @diaglogs = ( $diaglog1 );
		@scriptInfoArray = get_script_run_info(\@diaglogs);

		#DEBUG : uncomment next line to pause script after FW erase but BEFORE verification stage 2
		#msgbox('DEBUG: Waiting for User Input', "waiting for user to continue erase...");
		
		@drivesForSecondPass = getSecondPassDrives(\@scriptInfoArray);
		
		store_drive_signatures();
	}

	if (scalar(@drivesForSecondPass)) {
		$modified_script_name = update_script($block_erase_script, \@drivesForSecondPass, 0);
		if (! -e $modified_script_name) {
			log_mesg("Unable to create updated test script \"$modified_script_name\", Exiting\n",1);
			exit 1;
		}

		my @diaglogs2ndPass = ( run_script($modified_script_name) );
		push(@scriptInfoArray, get_script_run_info(\@diaglogs2ndPass));
	}
	#print_script_info(\@scriptInfoArray);
	return print_html_reports_per_device(\@scriptInfoArray);
	#print join(", ", @scriptInfoArray);
}

#edit_field : prompt user to edit a particular custom field
sub edit_field {
	my $customField =$_[0];
	open(CPERR, ">&STDERR");
	open(STDERR, ">$tmpdir/inputsession.$$");
	system("dialog --ok-label 'OK' --help-button --help-label Back --cancel-label Cancel --inputbox 'Editing Field \"@{[$customField->name]}\"' 0 0 \"@{[$customField->value]}\"");
	my $fieldValue = "";
	my $ret = $? >> 8;
	unless ($ret) {
		my $FILE;
		if (open(FILE, "$tmpdir/inputsession.$$")) {
			$fieldValue = join("", <FILE>);
			$fieldValue =~ s/^\s+|\s+$//g;
			if (length($fieldValue) > MAX_FIELD_VALUE_CHARS) {
				msgbox("ERROR", "Invalid Input: too many characters in string. Maximum characters = " . MAX_FIELD_VALUE_CHARS);
				$ret=3;
			}
			else {
				$_[0]->value($fieldValue);
			}
			close(FILE);
		}
		close(STDERR);
		open(STDERR, ">&CPERR");
	}
	return $ret;
}

#write_report_changes : replace all Custom Field tokens in HTML with the new values
#after they have been edited by the user
sub write_report_changes {
	my $filename = $_[0];
	my($filename_tmp, $dirs, $suffix) = fileparse($filename, qr/\.[^.]*/);
	my $tmpFilename = "$tmpdir/" . $filename_tmp . "_$$" . $suffix . ".tmp";
	my $fh;
	my $fhnew;
	if (!open($fh, "<", "$filename")) {
		msgbox('Cannot Open', "Cannot open report file: $filename");
		return;
	}
	if (!open($fhnew, ">", "$tmpFilename")) {
		msgbox('Cannot Open', "Cannot open temporary report file: $tmpFilename");
		close($fh);
		return;
	}
	while(<$fh>) {
		my $newValue = $_;
		if ($newValue =~ /<\!-- CUSTOM_VALUE\(([^)]+)\) (-->[^<]*<\/div>)/) {
			for my $customField (@{$_[1]}) {
				if ($1 eq $customField->name) {
					my $valueToWrite = encode_entities($customField->value);
					$newValue =~ s/$2/-->$valueToWrite<\/div>/g;
				}
			}
		}
		print $fhnew $newValue;
	}
	close($fh);
	close($fhnew);
	my $ret = system("cp -f \"$tmpFilename\" \"$filename\"");
	if ($ret) {
		msgbox('Cannot Copy', "Cannot copy temp file \"$tmpFilename\" to output file \"$filename\"");
		return;
	}
}
#is_custom_field_in_list : check if custom field is already added to list to prevent duplicates
sub is_custom_field_in_list {
	my $customFieldsList = $_[0];
	my $name = $_[1];
	for my $field (@$customFieldsList) {
		if ($field->name eq $name) {
			return 1;
		}
	}
	return 0;
}
#read_in_custom_fields : read in custom fields from HTML tags
sub read_in_custom_fields {
	my $report_name = $_[0];
	my $filename = "$_[2]/$report_name";
	my $customFieldsList = $_[1];
	my $fh;
	if (!open($fh, "<", "$filename")) {
		msgbox('Cannot Open', "Cannot open report file: $filename");
		return;
	}
	while(<$fh>) {
		if ($_ =~ /<\!-- CUSTOM_VALUE\(([^)]+)\) -->([^<]*)<\/div>/) {
			if (0 == is_custom_field_in_list($customFieldsList, $1)) {
				push(@$customFieldsList, CustomField->new(
						 name=>$1,
						 value=>$2));
			}
		}
	}
	close($fh);
}


#edit_reports : edit multiple reports at once
sub edit_reports {
	my @customFieldsList = ();
	my $reports = $_[0];
	for my $report (@$reports) {
		read_in_custom_fields($report, \@customFieldsList, $_[1]);
	}
	my $writeValues=0;
	my $cancelled=0;

	for (my $idx=0; $idx<scalar(@customFieldsList) && (0 == $cancelled); ) {
		#print "edit @{[$customField->name]}\n";
		my $oldValue=$customFieldsList[$idx]->value;

		if ((0 == $_[2]) && (length($oldValue) > 0)) {
			$idx++;
			next;
		}
		
		my $ret = edit_field($customFieldsList[$idx]);
		switch($ret) {
			case 0 {
				if ($oldValue ne $customFieldsList[$idx]->value) {
					$writeValues=1;
				}
				$idx++;
			}
			case 1 { 
				$cancelled=1;
				$writeValues=0;
			}
			case 2 {
				if ($idx>0) {
					$idx--;
				}
			}
		}
	}
	if ($writeValues) {
		my $values="";
		my $largestNameSize=0;
		for my $customField (@customFieldsList) {
			if (length($customField->name) > $largestNameSize) {
				$largestNameSize=length($customField->name);
			}
		}
		for my $customField (@customFieldsList) {
			$values .= sprintf("%-*s = \"%s\"\n", $largestNameSize, $customField->name, $customField->value);
		}
		my $ret = msgbox_ok_cancel('Report Changes', $values, 'Commit Changes', 'Go Back');
		unless ($ret) {
			for my $filename (@$reports) {
				write_report_changes("$_[1]/$filename", \@customFieldsList);
			}
		}
	}
	else {
		msgbox('No Changes', 'No Changes To Be Made');
	}
}
#edit_single_report : edit a single report
sub edit_single_report {
	my @reportList = ( $_[0] ); #report list os size=1
	edit_reports(\@reportList, $_[1], $_[2]);
}

#select_reports : select reports to edit
sub select_reports {
	my $reports_dir = $_[0];
	my @report_array = ();
	my @menu_opts = ('--cancel-label', 'Back', '--ok-label', 'Edit Blank Fields', '--extra-button', '--extra-label', 'Edit All Fields', '--menu');
	my $select_all_menu_option="";
	get_report_names_html($reports_dir, \@report_array);
	#because the custom field value can be different in multiple reports
	#this code was commented out because it is not feasible to allow user to edit all reports at once if
	#the values can differ. Instead we only allow a signle report to edited at a time
	#if (scalar(@report_array)) {
	#	$select_all_menu_option = "EDIT ALL REPORTS";
	#}
	my $menu_title = "Choose Erasure Single Report to edit (or ALL), pick whether to Edit only Blank Fields or All Fields:";
	my $loop = 1;
	while ( $loop ) {
		my $showAllFields=0;
		my $ret = show_menu2(\@report_array, \@menu_opts, $menu_title, 'REPORTS', $select_all_menu_option, \$showAllFields);
		if ($ret =~ /[0-9]+/) {
			if (0 == $ret) {
				edit_reports(\@report_array, $reports_dir, $showAllFields);
			}
			else {
				$ret--;
				if ($ret < scalar(@report_array)) {
					edit_single_report($report_array[$ret], $reports_dir, $showAllFields);
				}
			}
		}
		elsif ($ret eq 'c') {
			$loop=0;
		}
	}
}

#generate_final_reports : transform all reports in the tmp directory into PDF files in the output directory
sub generate_final_reports {
	my $reports_dir = $_[0];
	my @report_array = ();
	get_report_names_html($reports_dir, \@report_array);

	if (0==scalar(@report_array)) {
		if ($autogenreports) {
			log_mesg("NO REPORTS DETECTED\n");
		}
		else {
			msgbox('No Reports', 'NO REPORTS DETECTED');
		}
		return 1;
	}
	
	for my $reportHtml (@report_array) {
		my($filename, $dirs, $suffix) = fileparse($reportHtml, qr/\.[^.]*/);
		my $filenamePdf = sprintf("$outdir/%s.pdf", $filename);
		if (can_run('wkhtmltopdf')) {
			log_mesg("Generating report: $filenamePdf\n");
			system("wkhtmltopdf $reports_dir/$reportHtml $filenamePdf 1>/dev/null 2>&1");
		}
	}
	if ($autogenreports) {
		log_mesg("Final Reports Generated to output directory: $outdir\n");
	}
	else {
		msgbox('Final Report Generated', "Final Reports Generated to output directory: $outdir");
	}
	return 0;
}

#main_menu : the main menu system
#the "erase drives" option is removed if the the "--editreports" option is passed in
#to prevent the user from accidently initiating an erase again
sub main_menu {
	my @menu_list = ( 'Erase Drives', 'Edit Reports', 'Preview Reports', 'Generate Final Reports', 'View Final Reports' );
	my $OPT_ERASE_DRIVES = 1;
	my $OPT_SELECT_REPORTS = 2;
	my $OPT_PREVIEW_REPORTS = 3;
	my $OPT_GENERATE_REPORTS = 4;
	my $OPT_VIEW_REPORTS = 5;
	if ($editreportsonly) {
		splice(@menu_list, 0, 1); #remove 'Erase Drives' option
		$OPT_ERASE_DRIVES = 9999; #remove 'Erase Drives' option
		$OPT_SELECT_REPORTS = 1;
		$OPT_PREVIEW_REPORTS = 2;
		$OPT_GENERATE_REPORTS = 3;
		$OPT_VIEW_REPORTS = 4;
	}
	my @menu_opts = ('--ok-label', 'Select','--cancel-label', 'Exit', '--menu');
	my $loop = 1;
	while ( $loop ) {
		my $ret = show_menu2(\@menu_list, \@menu_opts, 'Welcome to PC-Doctor Drive Erase. Choose An Option', 'REPORTS');
		if ($ret =~ /[0-9]+/) {
			switch($ret) {
				case ($OPT_ERASE_DRIVES) { do_drive_erase(); }
				case ($OPT_SELECT_REPORTS) { select_reports($tmpdir) }
				case ($OPT_PREVIEW_REPORTS) { preview_reports($tmpdir) }
				case ($OPT_GENERATE_REPORTS) { generate_final_reports($tmpdir); }
				case ($OPT_VIEW_REPORTS) { view_reports($outdir); }
			}
		}
		elsif ($ret eq 'c') {
			$loop=0;
		}
	}
}

###################START MAIN WORKFLOW###################

log_mesg("Welcome to PC-Doctor Drive Erase\n");

my $retCode=0;
#auto erase if option is set
if ($autoerase) {
	$retCode = do_drive_erase();
}
#auto generate reports if option is set
if ($autogenreports) {
	$retCode = generate_final_reports($tmpdir);
}
#if either or both autgen options are set exit with presenting menu
if ($autoerase || $autogenreports) {
	exit $retCode;
}
#otherwise, launch the workflow menu system
main_menu();

END {
	log_mesg("Exiting Workflow Script\n");
}

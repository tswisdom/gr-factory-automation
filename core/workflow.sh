#!/usr/bin/env bash

# Factory Workflow Script Version 2.1
define_constants() {
    configure_script_tracing
    PS4='\D{%F %T}: $LINENO + '
    [[ ! -e /usr/local/pcdoctor/bin/Variables ]] && mkdir /usr/local/pcdoctor/bin/Variables
    SHARE="/mnt/reminst/"
    AZURE="http://gridfunc.blob.core.windows.net/input-prod?sv=2017-04-17&ss=bfqt&srt=sco&sp=rwdlacup&se=2018-12-31T22:16:59Z&st=2017-08-25T14:16:59Z&spr=https,http&sig=4FNakR8bhe8PAwxLPhyGjbvCOQ6OB2918v6KHohkT%2BA%3D"
    PCDDIR="/usr/local/pcdoctor/bin/"
    CPU="$(dmidecode --string processor-version)"
    evaluate_asset_type
    MANUFACTURER="$(dmidecode --string system-manufacturer)"
    MODEL="$(dmidecode --string system-product-name | awk '{$1=$1;print}')"
    SERIAL="$(dmidecode --string system-serial-number | awk '{$1=$1;print}')"
    [[ "{SERIAL}" == 'N/A' ]] && SERIAL="TEMPID"
    UUID="$(dmidecode -s system-uuid | awk '{$1=$1;print}')"
    NOTRUN="$(printf "\e[1m\e[38;5;226mNot-Run\e[0m")"
    FAIL="$(printf "\e[4m\e[1m\e[5m\e[38;2;255;0;5mFail\e[0m")"
    PASS="$(printf "\e[1m\e[38;2;0;255;5mPass\e[0m")"
    TRUE="$(printf "\e[1m\e[38;2;0;255;5mTrue\e[0m")"
    FALSE="$(printf "\e[4m\e[1m\e[5m\e[38;2;255;0;5mFalse\e[0m")"
    UNDETERMINED="$(printf "\e[1m\e[38;5;226mUndetermined\e[0m")"
    HDDCOUNT="$(lsblk --nodeps --include "8,259" --noheadings --output RM | grep --invert-match --ignore-case "1" | wc --lines)"
    WIFISSID="GlobalTest"
    WIFIPASS="GlobalGuest2016!"
    declare -x -g -i MissingParts="0"
    [[ "$HDDCOUNT" == 0 ]] && MissingParts+="1" && syncronize_variables MissingParts Note="HDD - NONE"
    wait_for_network && IP="$(ip route | awk '/default/ { print $3 }')"
    syncronize_variables PS4 IP SHARE AZURE PCDDIR CPU MANUFACTURER MODEL SERIAL UUID NOTRUN FAIL PASS TRUE FALSE UNDETERMINED HDDCOUNT
    return
}
define_variables() {
    declare -g -xu REPLY
    declare -g -xu AssetID
    declare -g -xu TechID
    declare -g -xu CosmeticGrade
    declare -g -xi ScreenSize
    [[ "$(ls /sys/class/backlight 2>> "${PCDDIR}Logging/tracelog" | wc -l)" == 0 ]] && FLR=false || FLR=true
    [[ "$(grep -q -c -m "1" "noxwindows" /proc/cmdline)" == "1" ]] && GUI=False || GUI=True
    declare -g -x KeyboardLanguage="EN-US"
    declare -g -x OSType="Not Installed"
    syncronize_variables REPLY AssetID TechID CosmeticGrade ScreenSize KeyboardLanguage OSType FLR GUI
    return
}
evaluate_asset_type() {
    CHASSIS="$(dmidecode --string chassis-type)"
    if [[ "$CHASSIS" =~ [[:alnum:]]*Desktop$|[[:alnum:]]*Tower$|[[:alnum:]]*Chassis$|Blade*[[:alnum:]]$|[[:alnum:]]*PC$ ]]; then
        HEADLESS="True"
        [[ "$CHASSIS" =~ [[:alnum:]]*Chassis$|Blade*[[:alnum:]]$ ]] && syncronize_variables SERVER="True"
    else
        HEADLESS="False"
    fi
    syncronize_variables HEADLESS CHASSIS
    return
}
invalid_model_check() {
    [[ "$(grep -x -c -m "1" "${MODEL^^}" "${SHARE}InvalidModels.txt")" != "0" || ! "$MODEL" ]] && syncronize_variables InvalidModel="True" MODEL="$(printf "\e[4m\e[1m\e[5m\e[38;2;255;0;5m${MODEL}\e[0m")"
    return
}
define_shell_behavior() {
    cd "${PCDDIR}"
    echo -ne "\033]0;GR Linux Factory Automation V2.1\007"
    echo -e "NVMeDeviceExclusionList.value=\nBlockTestExclusionList.value=" > libNVMeInfo.p5i && ${PCDDIR}resourceUtil import libNVMeInfo.p5i
    echo -e "EnableDriveWipeForNVMe.value=true\nEnableDriveWipeForSSDs.value=true" > libAtaInfo.p5i && ${PCDDIR}resourceUtil import libAtaInfo.p5i
    echo -e "EnableDriveWipeForNVMe.value=true\nEnableDriveWipeForSSDs.value=true" > libScsiInfo.p5i && ${PCDDIR}resourceUtil import libScsiInfo.p5i
    echo -e "EnableDriveWipeForNVMe.value=true\nEnableDriveWipeForSSDs.value=true" > pcdrsysinfostorage.p5i && ${PCDDIR}resourceUtil import pcdrsysinfostorage.p5i
    connect_wireless_network &
    restore_backlight
#    [[ "$GUI" != "False" ]] && normalize_screen_resolution
    mount_file_share
    import_resources
    invalid_model_check
    trap receive_input SIGHUP
    trap "save_variables exit" SIGTERM SIGINT SIGPWR
    return
}
restore_backlight() {
    for i in $(ls /sys/class/backlight 2>> "${PCDDIR}Logging/tracelog"); do
        { Bl_Power="$(cat /sys/class/backlight/${i}/bl_power)"; echo "1" > /sys/class/backlight/${i}/bl_power; echo "0" > /sys/class/backlight/${i}/bl_power; echo "$Bl_Power" > /sys/class/backlight/${i}/bl_power; }
        cat /sys/class/backlight/${i}/max_brightness > /sys/class/backlight/${i}/brightness
    done
    return
}
normalize_screen_resolution() {
    xrandr -s "$(xrandr | grep -o -m "1" -E "1280x[[:digit:]]{3,4}")"
    return
}
mount_file_share() {
    [[ ! -e "${SHARE}" ]] && mkdir "${SHARE}"
    wait_for_network && mount -t cifs //${IP}/reminst "${SHARE}" -o username=Administrator,password=Braker1 2>> "${PCDDIR}Logging/tracelog"
    return
}
import_resources() {
    cp -f -r ${SHARE}Test\ Scripts/* "${PCDDIR}scripts/custom"
    cp -f -r ${SHARE}Images/* "${PCDDIR}images"
    return
}
configure_script_tracing() {
    exec 19>> /usr/local/pcdoctor/bin/Logging/tracelog
    export BASH_XTRACEFD="19"
    set -a -x
    return
}
update_erase_drive_configuration() {
    echo -e "CustomField.1.Name=Erasure Provider
CustomField.1.Value=Global Resale
CustomField.2.Name=Technician ID
CustomField.2.Value=${TechID}
CustomField.3.Name=Asset ID
CustomField.3.Value=${AssetID}
remote_server_ip=${IP}
wipe_dco=true
wipe_hpa=true
tmp_dir=/tmp/erase_tmp_files
output_dir=${PCDDIR}testlogs
template_html=report_template.html
fw_erase_script=scripts/custom/firmware_erase_script.xml
block_erase_script=scripts/custom/${Passes:-single}_pass.xml
fw_verify_percentage=10
attempt_unfreeze=${FLR}" > ${PCDDIR}erase_config/erase_drive.cfg
    return
}
launch_input_terminal() {
    xterm -title "Input Terminal" -e "bash $0 input $$" &
    return
}
input() {
    [[ "$GUI" != "False" ]] && configure_script_tracing
    [[ "$GUI" != "False" ]] && for i in $(ls /usr/local/pcdoctor/bin/Variables 2>> "${PCDDIR}Logging/tracelog"); do source "/usr/local/pcdoctor/bin/Variables/${i}"; done
    display_message ProvideInput
    [[ ! "$AssetID" ]] && asset_id
    [[ ! "$TechID" ]] && tech_id
    [[ ! "$CosmeticGrade" ]] && cosmetic_grade
    if [[ "$HEADLESS" != "True" ]]; then
        [[ ! "$Webcam" ]] && webcam
        [[ ! "$ScreenSize" ]] && screen_size
        [[ ! "$KeyboardLanguage" ]] && keyboard_language
        [[ ! "$ACAdapter" ]] && ac_adapter
    fi
    [[ "$SERVER" == "True" && ! "$HDDBayCount" ]] && hdd_bay_count
    clear
    read -N "1" -t "10" -p "Would you like to provide any notes? [y/N]: "
    [[ "$REPLY" == "Y" ]] && notes manual
    [[ ! "$TechCut" ]] && techcut manual
    echo 'declare -g -x Input="True"' > "${PCDDIR}Variables/Input"
    kill -1 "$1"
    exit
}
receive_input() {
    for i in $(ls ${PCDDIR}Variables 2>> "${PCDDIR}Logging/tracelog"); do source "${PCDDIR}Variables/${i}"; done
    upload_logs
    return
}
display_status() {
    echo -e "Asset ID: ${AssetID:-Undetermined}      Technician ID: ${TechID:-Undetermined}
Manufacturer: $MANUFACTURER      Model: $MODEL      CPU: $CPU

Cosmetic Grade: ${CosmeticGrade:-Undetermined}      Functional Grade: ${FunctionalGrade:-Undetermined}      Inventory Status: ${AssetStatus:-Undetermined}
Auxiliary Field 1: ${AuxField1:-Empty}      Auxiliary Field 2: ${AuxField2:-Empty}      Notes: ${Notes:-Empty}"
    [[ "$HEADLESS" != "True" ]] && echo -e -n "Keyboard Language: ${KeyboardLanguage:-Undetermined}      "
    [[ "$HEADLESS" != "True" ]] && echo -e -n "Screen Size: ${ScreenSize:-Undetermined}\"       "
    [[ "$HEADLESS" != "True" ]] && echo -e -n "AC-Adapter: ${ACAdapter:-Undetermined}       "
    [[ "$SERVER" != "True" ]] && echo -e -n "OS Type: ${OSType:-"Not Installed"}      "
    [[ "$HEADLESS" != "True" ]] && echo -e -n "Webcam: ${Webcam:-Undetermined}       "
    [[ "$SERVER" == "True" ]] && echo -e -n "Hard Drive Bay Count: ${HDDBayCount:-Undetermined}       "
    echo -e "\n"
    echo -e -n "HDD Health: ${SmartTest:-${NOTRUN}}      Battery Test: ${BatteryTest:-${NOTRUN}}      Erasure: ${Erasure:-${NOTRUN}}      Diagnostics: ${Diagnostic:-${NOTRUN}}
System Information Uploaded: ${SYSINFOUpload:-${FALSE}}   "
    [[ "$Erasure" ]] && echo -e -n "Erasure Log Uploaded: ${WIPEUpload:-${FALSE}}   "
    [[ "$Diagnostic" ]] && echo -e -n "Diagnostic Log Uploaded: ${DIAGUpload:-${FALSE}}   "
    echo -e -n "GR Log Uploaded: ${GRVariablesUpload:-${FALSE}}"
    [[ "$Diagnostic" == "${FAIL}" ]] && display_failed_components
    [[ "$InvalidModel" == "True" ]] && echo -e "\n\n\e[5m\e[38;2;255;0;5mThis system contains invalid model information.\nPlease update the model manually in the GRID after integration has completed.\e[0m"
    return
}
test_complete() {
    clear
    echo -e "Test Complete:\n"
    display_status
    display_message TestComplete
    echo -e "\n"
    [[ "$GUI" == False ]] && input $$
    functional_grade
    asset_status
    [[ "$Erasure" == "${PASS}" && "$TechCut" == "True" && "$Diagnostic" == "${PASS}" ]] && apply_image "0" || generate_grvariables_file
    main_menu
}
display_message() {
    [[ "$GUI" != "False" ]] && feh -FZ "${PCDDIR}images/${1}.tif" &
    return
}
display_failed_components() {
    echo -e -n "\n\n$(printf "\e[5m\e[38;2;255;0;5mFunctional testing has failed for the following components: \e[0m")"
    grep "\"Failed\"" ${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv | sort | uniq -w "10" | grep -o ^\"[[:alpha:]]*[[:space:]]*[[:alpha:]]*: | sed -e "s/^\"//" -e "s/://" > "${PCDDIR}Logging/FailedComponents"
    while read i; do echo -e -n "\e[5m\e[38;2;255;0;5m${i}  \e[0m"; done < "${PCDDIR}Logging/FailedComponents"
    return
}
main_menu() {
    [[ "$Input" != "True" && -e ${PCDDIR}Variables/Input ]] && receive_input
    clear
    echo -e "Main Menu:\n"
    display_status
    echo -e "\n\n1) Run PC Doctor Script.
2) Apply Image.
3) Change Asset Configuration.
4) Manage PC Doctor Logs.
5) Exit.
6) Shutdown.\n"
    read -N "1" -p "Select an option [1-6]: "
    case "$REPLY" in
        1) pcd_menu ;;
        2) apply_image ;;
        3) asset_configuration_menu ;;
        4) manage_logs_menu ;;
        5) save_variables exit ;;
        6) poweroff ;;
        *) main_menu ;;
    esac
}
pcd_menu() {
    clear
    echo -e "1) Run Wipe.
2) Run Diagnostics.
3) Run PC Doctor Without a Script. (RESULTS WILL NOT BE SAVED)
4) Return to Main Menu.\n"
    read -N "1" -p "Select an option. [1-4]: "
    case "$REPLY" in
        1) wipe
        pcd_menu ;;
        2) diagnostics
        pcd_menu ;;
        3) cd "${PCDDIR}"
        ./pcdgui run -s "$IP"
        pcd_menu ;;
        4) main_menu ;;
        *) pcd_menu ;;
    esac
}
apply_image() {
    [[ ! "$1" ]] && clear
    [[ "$MANUFACTURER" == "Apple Inc." ]] && OSType="Mac OS El Capitan" || OSType="MAR CU Windows 10 Pro, 64 Bit"
    syncronize_variables OSType GRVariablesUpload="" Note="OS Loaded"
    [[ "$1" ]] && echo -e "\n\nThis machine meets all requirements for imaging."
    echo -e "Please reboot the system and install ${OSType}."
    read -n "1" -p "Press \"R\" to reboot or any other key to continue."
    generate_grvariables_file
    [[ "$REPLY" == R ]] && poweroff -r
    return
}
asset_configuration_menu() {
    clear
    echo -e "Asset Configuration Menu:

0) Asset ID: ${AssetID:-"Undetermined"}.
1) Technician ID: ${TechID:-Undetermined}
2) Functional Grade: ${FunctionalGrade:-"Undetermined"}.
3) Inventory Status: ${AssetStatus:-"Undetermined"}.
4) Cosmetic Grade: ${CosmeticGrade:-"Undetermined"}.
5) Auxiliary Field 1: ${AuxField1:-"Empty"}.
6) Auxiliary Field 2: ${AuxField2:-"Empty"}.
7) Notes: ${Notes:-"Empty"}.
8) Attribute Values.
9) Return to main menu.\n"
    read -N "1" -p "Please select an option. [1-9]: "
    case "$REPLY" in
        0) asset_id
        upload_logs
        asset_configuration_menu ;;
        1) tech_id
        upload_logs
        asset_configuration_menu ;;
        2) clear
        functional_grade
        asset_configuration_menu ;;
        3) clear
        asset_status
        asset_configuration_menu ;;
        4) cosmetic_grade
        asset_configuration_menu ;;
        5) auxiliary_field_1
        asset_configuration_menu ;;
        6) auxiliary_field_2
        asset_configuration_menu ;;
        7) notes
        asset_configuration_menu ;;
        8) attribute_value_menu
        asset_configuration_menu ;;
        9) [[ "$GRVariablesUpload" != "${TRUE}" ]] && generate_grvariables_file
        main_menu ;;
        *) asset_configuration_menu ;;
    esac
}
attribute_value_menu() {
    clear
    echo -e "Asset Configuration Menu:

1) AC Adapter: ${ACAdapter:-"Undetermined"}.
2) Screen Size: ${ScreenSize:-"Undetermined"}.
3) Webcam: ${Webcam:-"Undetermined"}.
4) Keyboard Language: ${KeyboardLanguage:-"Undetermined"}.
5) Hard drive bay count: ${HDDBayCount:-"Undetermined"}.
6) Return to asset configuration menu.\n"
    read -N "1" -p "Select an option [1-6]: "
    case "$REPLY" in
        1) ac_adapter
        attribute_value_menu ;;
        2) screen_size
        attribute_value_menu ;;
        3) webcam
        attribute_value_menu;;
        4) keyboard_language
        attribute_value_menu;;
        5) hdd_bay_count
        attribute_value_menu ;;
        6) asset_configuration_menu ;;
        *) attribute_value_menu ;;
    esac
}
manage_logs_menu() {
    clear
    echo -e "1) View logs.
2) Re-upload logs.
3) Return to main menu.\n"
    read -N "1" -p "Please select an option. [1-3]: "
    case "$REPLY" in
        1) view_logs_menu
        manage_logs_menu ;;
        2) reupload_logs_menu
        manage_logs_menu ;;
        3) main_menu ;;
        *) manage_logs_menu ;;
    esac
}
view_logs_menu() {
    clear
    echo -e "1) System information log."
    [[ -e "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SMART.csv" ]] && echo -e "2) Hard drive health log." || echo -e "X) Hard drive health log is unavailable."
    [[ -e "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-WIPE-DISK1.pdf" ]] && echo -e "3) Hard drive wipe log." || echo -e "X) Hard drive wipe log is unavailable."
    [[ -e "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv" ]] && echo -e "4) System diagnostics log." || echo -e "X) System diagnostics log is unavailable."
    echo -e "5) Return to manage logs.\n"
    read -N "1" -p "Please select an option. [1-5]: "
    case "$REPLY" in
        1) [[ -e "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SYSINFO.yaml" ]] && nano "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SYSINFO.yaml" || ( sysinfo && nano "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SYSINFO.yaml" )
        view_logs_menu ;;
        2) [[ -e "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SMART.csv" ]] && nano "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SMART.csv" || echo -e "Hard drive health log is unavailable."
        view_logs_menu ;;
        3) [[ -e "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-WIPE-DISK1.pdf" ]] && xpdf "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-WIPE*.pdf" || echo -e "Hard drive wipe log is unavailable."
        view_logs_menu ;;
        4) [[ -e "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv" ]] && nano "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv" || echo -e "System diagnostics log is unavailable."
        view_logs_menu ;;
        5) manage_logs_menu ;;
        *) view_logs_menu ;;
    esac
}
reupload_logs_menu() {
    clear
    echo -e "1) System information log."
    [[ -e "${PCDDIR}testlogs/${AssetID}-SMART.csv" ]] && echo -e "2) Hard drive health log." || echo -e "X) Hard drive health log is unavailable."
    [[ -e "${PCDDIR}testlogs/${AssetID}-WIPE-DISK1.pdf" ]] && echo -e "3) Hard drive wipe log." || echo -e "X) Hard drive wipe log is unavailable."
    [[ -e "${PCDDIR}testlogs/${AssetID}-DIAG.csv" ]] && echo -e "4) System diagnostics log." || echo -e "X) System diagnostics log is unavailable."
    echo -e "5) GRID Variables log.
6) Return to manage logs.\n"
    [[ "$AssetID" ]] || asset_id
    read -N "1" -p "Please select an option. [1-6]: "
    case "$REPLY" in
        1) [[ -e "${PCDDIR}testlogs/${AssetID}-SYSINFO.yaml" ]] && azure_upload SYSINFO || sysinfo
        reupload_logs_menu ;;
        2) [[ -e "${PCDDIR}testlogs/${AssetID}-SMART.csv" ]] && azure_upload SMART || echo -e "Hard drive health log is unavailable."
        reupload_logs_menu ;;
        3) [[ -e "${PCDDIR}testlogs/${AssetID}-WIPE-DISK1.pdf" ]] && azure_upload WIPE || echo -e "Hard drive wipe log is unavailable."
        reupload_logs_menu ;;
        4) [[ -e "${PCDDIR}testlogs/${AssetID}-DIAG.csv" ]] && azure_upload DIAG || echo -e "System diagnostics log is unavailable."
        reupload_logs_menu ;;
        5) generate_grvariables_file
        reupload_logs_menu ;;
        6) manage_logs_menu ;;
        *) reupload_logs_menu ;;
    esac
}
poweroff() {
    save_variables
    [[ "$1" ]] && shutdown "${1}" now || shutdown now
}
azure_upload() {
    [[ "${1}" == "WIPE" ]] && FileCount="$(( $HDDCOUNT + 1 ))" || FileCount="1"
    wait_for_network && grep -q -c -m "1" "Total files transferred: ${FileCount}" <(azcopy --quiet --recursive --source "${PCDDIR}testlogs" --destination "${AZURE}" --include "${AssetID}-${1}*") >> "${PCDDIR}Logging/tracelog" 2>&1
    [[ "$?" != "0" ]] && { rm -r /root/Microsoft/Azure/AzCopy/*; azure_upload "${1}"; } || syncronize_variables ${1}Upload="${TRUE}"
    return "0"
}
upload_logs() {
    update_logs
    [[ -e "${PCDDIR}testlogs/${AssetID}-SYSINFO.yaml" && "$SYSINFOUpload" != "${TRUE}" ]] && { echo -e "TechnicianUserName: $TechID" >> "${PCDDIR}testlogs/${AssetID}-SYSINFO.yaml"; azure_upload SYSINFO; }
    [[ -e "${PCDDIR}testlogs/${AssetID}-SMART.csv" && "$SMARTUpload" != "${TRUE}" ]] && { echo -e "TechnicianUserName: $TechID" >> "${PCDDIR}testlogs/${AssetID}-SMART.csv"; azure_upload SMART; }
    if [[ -e "/tmp/erase_tmp_files/${AssetID}-WIPE-DISK1.html" && "$WIPEUpload" != "${TRUE}" ]]; then
        echo -e "TechnicianUserName: $TechID\nErasureStandard: NIST Clear\nVerificationPercentage: 100" >> "${PCDDIR}testlogs/${AssetID}-WIPE.csv"
        ${PCDDIR}drive_erase_workflow.pl --autogenreports >> "${PCDDIR}Logging/tracelog" 2>&1
        [[ "$?" != 0 ]] && read -n "1" -p "There was an error generating the erasure certificate. Please inform a team lead."
        azure_upload WIPE
    fi
    [[ -e "${PCDDIR}testlogs/${AssetID}-DIAG.csv" && "$DIAGUpload" != "${TRUE}" ]] && { echo -e "TechnicianUserName: $TechID" >> "${PCDDIR}testlogs/${AssetID}-DIAG.csv"; azure_upload DIAG; }
    [[ "${AssetID}" && "$GRVariablesUpload" != "${TRUE}" && "$GRVariables" == "True" ]] && generate_grvariables_file
    return
}
update_logs() {
    [[ "$(ls /tmp/erase_tmp_files/${SERIAL}* 2>> "${PCDDIR}Logging/tracelog" | wc -l)" != "0" && "$AssetID" ]] && rename.ul "${SERIAL}" "${AssetID}" /tmp/erase_tmp_files/${SERIAL}-WIPE*html
    [[ "$(ls ${PCDDIR}testlogs/${SERIAL}* 2>> "${PCDDIR}Logging/tracelog" | wc -l)" != "0" && "$AssetID" ]] && rename.ul "${SERIAL}" "${AssetID}" ${PCDDIR}testlogs/${SERIAL}* # Assigns asset ID to any outputs completed prior to providing ID
    [[ "$AssetIDOld" && "$(ls ${PCDDIR}testlogs/${AssetIDOld}* 2>> "${PCDDIR}Logging/tracelog" | wc -l)" != "0" ]] && rename.ul "${AssetIDOld}" "${AssetID}" ${PCDDIR}testlogs/${AssetIDOld}* # Assigns asset ID to any outputs completed prior to providing ID
    [[ -e /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}-WIPE-DISK1.html && "$AssetID" ]] && sed -i "s/<\!-- CUSTOM_VALUE(Asset ID) -->*<\/div>/<\!-- CUSTOM_VALUE(Asset ID) -->${AssetID}<\/div>/" /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}*html 2>> "${PCDDIR}Logging/tracelog"
    [[ -e /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}-WIPE-DISK1.html && "$TechID" ]] && sed -i "s/<\!-- CUSTOM_VALUE(Technician ID) -->*<\/div>/<\!-- CUSTOM_VALUE(Technician ID) -->${TechID}<\/div>/" /tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}*html 2>> "${PCDDIR}Logging/tracelog"
    return
}
save_variables() {
    declare -p AssetID TechID CosmeticGrade CosmeticGradeCode FunctionalGrade FunctionalGradeCode AssetStatus AssetStatusCode AuxField1 AuxField2 Notes Sysinfo SYSINFOUpload BatteryTest SmartTest SMARTUpload Erasure WIPEUpload Diagnostic DIAGUpload GRVariables GRVariablesUpload OSType KeyboardLanguage ACAdapter ScreenSize Webcam HDDBayCount Input > "${PCDDIR}Logging/${SERIAL}-${UUID}-Variables.txt" 2>> "${PCDDIR}Logging/tracelog"
    sed -i "s/declare/declare -g/" "${PCDDIR}Logging/${SERIAL}-${UUID}-Variables.txt" 2>> "${PCDDIR}Logging/tracelog"
    wait_for_network && azcopy --quiet --recursive --source "${PCDDIR}Logging" --destination "${AZURE}" --include "${SERIAL}-${UUID}-Variables.txt" >> "${PCDDIR}Logging/tracelog" 2>&1
    [[ "$?" != 0 ]] && { rm -r /root/Microsoft/Azure/AzCopy/*; save_variables "${1}"; }
    [[ "${1}" ]] && "${1}"
    return
}
asset_lookup() {
    for i in $(ls ${PCDDIR}Variables 2>> "${PCDDIR}Logging/tracelog"); do source "${PCDDIR}Variables/${i}"; done
    wait_for_network && azcopy --quiet --recursive --source "${AZURE}" --destination "${PCDDIR}Logging" --include "${SERIAL}-${UUID}-Variables.txt" >> "${PCDDIR}Logging/tracelog" 2>&1
    [[ "$?" != 0 ]] && { rm -r /root/Microsoft/Azure/AzCopy/*; asset_lookup; }
    [[ -e "${PCDDIR}Logging/${SERIAL}-${UUID}-Variables.txt" ]] && source "${PCDDIR}Logging/${SERIAL}-${UUID}-Variables.txt" && ( declare IFS="$(printf '\n')"; while read i; do echo "${i}" > ${PCDDIR}Variables/$(echo "$i" | grep -o [[:graph:]]*= | sed -e "s/=//"); done < <(cat ${PCDDIR}Logging/${SERIAL}-${UUID}-Variables.txt); )
    [[ "$AssetID" ]] && wait_for_network && azcopy --exclude-older --exclude-newer --quiet --recursive --source "${AZURE}" --destination "${PCDDIR}testlogs" --include "${AssetID}" >> "${PCDDIR}Logging/tracelog" 2>&1
    return
}
sysinfo() {
    clear
    echo -e "Generating system information log, please wait..."
    wait_for_network && ${PCDDIR}pcd sysinfo -ep in -s "${IP}" > "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SYSINFO.yaml"
    [[ "$?" != 0 || "$(grep -s -c -m "1" "^Device: System - System$" ${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SYSINFO.yaml)" != 1 ]] && sysinfo || Sysinfo="${PASS}"
    syncronize_variables Sysinfo SYSINFOUpload=""
    [[ "${AssetID}" ]] && azure_upload SYSINFO
    return
}
battery_test() {
    declare -i ReturnCode
    if [[ "$CHASSIS" =~ Laptop|Notebook|Tablet|Convertible|Detachable|Portable ]]; then
        clear
        echo -e "Performing battery test, please wait."
        wait_for_network && ${PCDDIR}pcd run -ep in -s "${IP}" -t BatteryLifeTest >> "${PCDDIR}Logging/tracelog" 2>&1
        ReturnCode="$?"
        if [[ "$ReturnCode" == 23 ]]; then
            syncronize_variables BatteryTest="${FAIL}" AuxField2="BULK"
        elif [[ "$ReturnCode" == 31 ]]; then
            MissingParts+="1"
            syncronize_variables BatteryTest="Not-Installed" MissingParts AuxField2="BULK" Note="BATTERY - NONE"
        elif [[ "$ReturnCode" == 0 || "$ReturnCode" == 51 ]]; then
            syncronize_variables BatteryTest="${PASS}"
        fi
    else
        syncronize_variables BatteryTest='N/A'
    fi
    return
}
syncronize_variables() {
    while [[ "$#" -gt 0 ]]; do
        [[ "${1}" != "${1%%=*}" ]] && declare -g -x "${1}"
        if [[ "${1%%=*}" != "Note" ]]; then
            declare -p ${1%%=*} > "${PCDDIR}Variables/${1%%=*}" 2>> "${PCDDIR}Logging/tracelog"
            sed -i "s/declare/declare -g/" "${PCDDIR}Variables/${1%%=*}" 2>> "${PCDDIR}Logging/tracelog"
        else
            [[ -e "${PCDDIR}Variables/Notes" ]] && source "${PCDDIR}Variables/Notes"
            declare -g -x Notes="$(echo -e "${Note} ${Notes}" | awk '{$1=$1;print}')"
            echo "declare -g -x Notes=\"${Notes}\"" > "${PCDDIR}Variables/Notes" 2>> "${PCDDIR}Logging/tracelog"
        fi
        shift
    done
    return "0"
}
smart_test() {
    clear
    echo -e "Performing hard drive health test, please wait."
    wait_for_network && [[ "$GUI" != False ]] && ${PCDDIR}pcdgui run -s "${IP}" -ef -v -f "${PCDDIR}scripts/custom/smarttest.xml" -i "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SMART.csv" || ${PCDDIR}pcd run -s "${IP}" -ef -v -f "${PCDDIR}scripts/custom/smarttest.xml" -i "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SMART.csv"
    [[ "$?" != 23 ]] && SmartTest="${PASS}" || SmartTest="${FAIL}" Note="HARD DRIVE - FAIL"
    syncronize_variables Note SmartTest SMARTUpload=""
    [[ "${AssetID}" ]] && azure_upload SMART
    return
}
passes() {
    clear
    read -N "1" -t "10" -p "Please select whether to run a single pass or triple pass wipe. [1/3]: " Passes
    [[ "$Passes" == 3 ]] && { Passes=triple; update_erase_drive_configuration; } || Passes=single
    syncronize_variables Passes
    return
}
wipe() {
    clear
    [[ "$Passes" ]] || passes
    echo -e "Performing hard drive wipe, please wait."
    wait_for_network && ${PCDDIR}drive_erase_workflow.pl --autoerase 2>> "${PCDDIR}Logging/tracelog"
    [[ "$?" == 0 ]] && Erasure="${PASS}" || Erasure="${FAIL}" Note="HARD DRIVE - FAIL"
    restore_backlight
    [[ "$(ls /tmp/erase_tmp_files/$(date +%F)*html 2>> "${PCDDIR}Logging/tracelog")" ]] && declare -i Count="0" && until [[ "$Count" == "$HDDCOUNT" ]]; do for i in /tmp/erase_tmp_files/$(date +%F)*html; do Count+="1"; mv "${i}" "/tmp/erase_tmp_files/${AssetID:-"${SERIAL}"}-WIPE-DISK${Count}.html"; done; done #change naming convention for wipe report
    create_wipe_service_tag_log
    syncronize_variables Note Erasure WIPEUpload=""
    if [[ "${AssetID}" ]]; then
        update_logs
        echo -e "TechnicianUserName: $TechID\nErasureStandard: NIST Clear\nVerificationPercentage: 100" >> "${PCDDIR}testlogs/${AssetID}-WIPE.csv"
        ${PCDDIR}drive_erase_workflow.pl --autogenreports >> "${PCDDIR}Logging/tracelog" 2>&1
        [[ "$?" != 0 ]] && read -n "1" -p "There was an error generating the erasure certificate. Please inform a team lead."
        azure_upload WIPE
    fi
    return
}
create_wipe_service_tag_log() {
    echo -e -n "\"PC-Doctor Factory for Linux\",\"v6.4.7050.937\"
\"1\",\"1\",\"$(date +"%F %T")\",\"$(date +"%F %T")\"
\"Hard Drive: \",\"Storage Utilities: Drive Wipe\"," > "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-WIPE.csv"
    if [[ "$Erasure" != "${PASS}" ]]; then
        echo -e "\"Failed\"\n" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-WIPE.csv"
    else
        echo -e "\"Completed\"\n" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-WIPE.csv"
    fi
    return
}
diagnostics() {
    clear
    echo -e "Performing system diagnostics, please wait."
    if [[ "${HEADLESS}" == "True" ]]; then
        wait_for_network && [[ "$GUI" != False ]] && ${PCDDIR}pcdgui run -s "${IP}" -ef -v -f "${PCDDIR}scripts/custom/diagnostics_headless.xml" -i "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv" 2>> "${PCDDIR}Logging/tracelog" || ${PCDDIR}pcd run -s "${IP}" -ef -v -f "${PCDDIR}scripts/custom/diagnostics_headless.xml" -i "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv"
    else
        wait_for_network && [[ "$GUI" != False ]] && ${PCDDIR}pcdgui run -s "${IP}" -ef -v -f "${PCDDIR}scripts/custom/diagnostics.xml" -i "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv" 2>> "${PCDDIR}Logging/tracelog" || ${PCDDIR}pcd run -s "${IP}" -ef -v -f "${PCDDIR}scripts/custom/diagnostics.xml" -i "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv"
    fi
    [[ "$?" =~ 23|29 ]] && syncronize_variables Diagnostic="${FAIL}" Note="DIAGNOSTICS - FAIL" || syncronize_variables Diagnostic="${PASS}" Note="DIAGNOSTICS - PASS"
    [[ "$(grep -q -c -m "1" "Hard Drive" <(grep "\"Failed\"" ${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-DIAG.csv))" == 1 ]] && syncronize_variables Note="HARD DRIVE - FAIL"
    syncronize_variables DIAGUpload=""
    [[ "${AssetID}" ]] && azure_upload DIAG
    return
}
generate_grvariables_file() {
    clear
    echo -e "The following values will be uploaded to the GRID.
If any of these values are missing or incorrect, please make any necessary updates manually.\n"
    display_status
    echo -e "TechnicianUserName: $TechID
AssetStatus: $AssetStatusCode
CosmeticGrade: $CosmeticGradeCode
FunctionalGrade: $FunctionalGradeCode" > "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$AuxField1" ]] && echo -e "AuxField1: $AuxField1" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$AuxField2" ]] && echo -e "AuxField2: $AuxField2" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$Notes" ]] && echo -e "Notes: $Notes" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$Notes" ]] && echo -e "Comments: $Notes" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$Webcam" ]] && echo -e "Webcam: $Webcam" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$KeyboardLanguage" ]] && echo -e "KeyboardLanguage: $KeyboardLanguage" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$ScreenSize" ]] && echo -e "ScreenSize: $ScreenSize" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$ACAdapter" ]] && echo -e "ACAdapter: $ACAdapter" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$OSType" ]] && echo -e "OSType: $OSType" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    [[ "$HDDBayCount" ]] && echo -e "HDDBayCount: $HDDBayCount" >> "${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-GRVariables.yaml"
    syncronize_variables GRVariables="True"
    [[ "${AssetID}" ]] && azure_upload GRVariables
    [[ "$?" == "0" ]] && clear && echo -e "The following values will be uploaded to the GRID.\nIf any of these values are missing or incorrect, please make any necessary updates manually.\n" && display_status
    echo -e "\n"
    read -n "1" -p "Press any key to continue."
    return
}
asset_id() {
    clear
    [[ "$AssetID" ]] && AssetIDOld="$AssetID"
    read -N "10" -p "Please scan asset ID: " AssetID
    AssetID="$(echo -e "$AssetID" | awk '{$1=$1;print}')"
    [[ "$AssetID" =~ (^[[:digit:]]{10}$)|(^U[SK][[:digit:]]{8}$) ]] || asset_id
    update_logs
    update_erase_drive_configuration
    syncronize_variables AssetID SYSINFOUpload="" SMARTUpload="" WIPEUpload="" DIAGUpload="" GRVariablesUpload=""
    return
}
tech_id() {
    clear
    read -p "Please scan technician ID, then press enter: " TechID
    TechID="$(echo -e "$TechID" | awk '{$1=$1;print}')"
    [[ "$TechID" =~ ^[[:upper:]]+\.[[:upper:]]*@GLOBALRESALE\.COM$ ]] || tech_id
    syncronize_variables TechID SYSINFOUpload="" SMARTUpload="" WIPEUpload="" DIAGUpload="" GRVariablesUpload=""
    update_logs
    update_erase_drive_configuration
    record_metric_user_touch
    return
}
cosmetic_grade() {
    clear
    read -N "1" -p "Please specify cosmetic grade. [A-D]: " CosmeticGrade
    case "$CosmeticGrade" in
        A) CosmeticGradeCode="2" ;;
        B) CosmeticGradeCode="3" ;;
        C) CosmeticGradeCode="4" ;;
        D) CosmeticGradeCode="5" ;;
        *) cosmetic_grade ;;
    esac
    syncronize_variables CosmeticGrade CosmeticGradeCode GRVariablesUpload=""
    return
}
functional_grade() {
    read -N "1" -p "Please specify a functional grade. [1-9]: " FunctionalGrade
    [[ "$FunctionalGrade" =~ ^[[:digit:]]$ ]] || functional_grade
    FunctionalGradeCode="$(( "$FunctionalGrade" + 15 ))"
    syncronize_variables FunctionalGrade FunctionalGradeCode GRVariablesUpload=""
    echo -e "\n"
    return
}
asset_status() {
    echo -e "1) Batching
2) Retail Ready
3) Repair
4) Imaging\n"
    read -N "1" -p "Please specify asset inventory status. [1-4]: "
    case "$REPLY" in
        1) AssetStatus="Batching" AssetStatusCode="11" ;;
        2) AssetStatus="Retail Ready" AssetStatusCode="7" ;;
        3) AssetStatus="Repair" AssetStatusCode="5" ;;
        4) AssetStatus="Imaging" AssetStatusCode="6" ;;
        *)  asset_status
    esac
    syncronize_variables AssetStatus AssetStatusCode GRVariablesUpload=""
    return
}
auxiliary_field_1() {
    clear
    read -p "Please enter data to be added to auxiliary field 1 and press enter: " AuxField1
    syncronize_variables AuxField1 GRVariablesUpload=""
    return
}
auxiliary_field_2() {
    clear
    read -p "Please enter data to be added to auxiliary field 2 and press enter: " AuxField2
    syncronize_variables AuxField2 GRVariablesUpload=""
    return
}
notes() {
    clear
    [[ "$Notes" ]] && echo -e "Notes: $Notes" || echo -e "The notes field is currently empty."
    if [[ "$Notes" && ! "${1}" ]]; then
        read -N "1" -p "Would you like to clear the notes field? [y/N]: "
        [[ "$REPLY" == Y ]] && Notes="" && rm "${PCDDIR}Variables/Notes"
    fi
    clear
    read -p "Please enter data to be added to the notes field and press enter: " Note
    syncronize_variables Note GRVariablesUpload=""
    return
}
techcut() {
    [[ "$BatteryTest" ]] || battery_test
    [[ ! "$TechCut" && "$MANUFACTURER" == "Apple Inc." || "$MANUFACTURER" == "Alienware" ]] && TechCut="True"
    [[ ! "$TechCut" && "$CosmeticGrade" =~ [CD] ]] && TechCut="False"
    [[ ! "$TechCut" && "$SERVER" == "True" ]] && TechCut="False"
    [[ ! "$TechCut" && "$BatteryTest" == "${FAIL}" || "$BatteryTest" == "Not-Installed" ]] && TechCut="False"
    [[ ! "$TechCut" ]] && (( MissingParts > 0 )) && TechCut="False"
    [[ ! "$TechCut" && "$(echo -e "$CPU" | grep -o -E '(Atom|Celeron|Dual-Core|Core\(TM\) M|Core\(TM\) m|Core\(TM\) Duo |Core\(TM\) i3|Pentium|Xeon|Opteron|Sempron|Athlon|Turion|Phenom|Radeon|AMD V|AMD G|AMD C|AMD E|')" ]] && TechCut="False"
    [[ ! "$TechCut" && "$(dmidecode -s processor-family)" =~ "Core i7"|"Core i5"|"Core i3" && "$CosmeticGrade" =~ [AB] ]] && TechCut="True"
    [[ ! "$TechCut" && "$(echo -e "$CPU" | grep -o -E '^AMD A(6|7|8|9|10|11|12)')" && "$CosmeticGrade" =~ [AB] ]] && TechCut="True"
    [[ ! "$TechCut" && "$(echo -e "$CPU" | grep -o -E '^AMD A(4|5)')" && ( "$CHASSIS" =~ ^[[:alnum:]]*Tower$ || "$CHASSIS" =~ ^[[:alnum:]]*Chassis$ ) && "$CosmeticGrade" =~ [AB] ]] && TechCut="True"
    if [[ ! "$TechCut" && "$1" == "manual" ]]; then
        clear
        [[ "${CosmeticGrade}" ]] && echo -e "This machine has been cosmetically graded a ${CosmeticGrade}." || cosmetic_grade
        echo -e "This machine contains the following CPU: ${CPU}\n"
        read -N "1" -p "Does this machine fall within tech cuts? [Y/N]: "
        [[ "$REPLY" =~ [YN] ]] || techcut manual
        [[ "${REPLY}" == "Y" ]] && TechCut="True" || TechCut="False"
    fi
    syncronize_variables TechCut
    return
}
ac_adapter() {
    clear
    read -N "1" -p "Does this machine include an AC Adapter? [Y/N]: "
    [[ "$REPLY" =~ [YN] ]] || ac_adapter
    [[ "$REPLY" == Y ]] && ACAdapter="ADAPTER INCLUDED" || ACAdapter="NO ADAPTER" Note="ADAPTER - NONE"
    syncronize_variables ACAdapter Note GRVariablesUpload=""
    return
}
keyboard_language() {
    clear
    echo -e "1) EN-US [English United States]
2) EN-GB [English United Kingdom]
3) ES-ES [Spanish Traditional]\n"
    read -N "1" -p "Please specify the keyboard language. [1-3]: "
    case "$REPLY" in
        1)  KeyboardLanguage="EN-US" ;;
        2)  KeyboardLanguage="EN-GB" ;;
        3)  KeyboardLanguage="EN-ES" ;;
        *)  keyboard_language ;;
    esac
    syncronize_variables KeyboardLanguage GRVariablesUpload=""
    return
}
screen_size() {
    read Width nul Height < <(xrandr | grep -o -E "[[:digit:]]{2,3}mm x [[:digit:]]{2,3}mm$")
    ScreenSizeFloat="$(printf '%.*f\n' "1" "$(echo -e "sqrt ( ( ( ${Height%mm} / 25.4 ) ^ 2 ) + ( ( ${Width%mm} / 25.4 ) ^ 2 ) )" | bc -l)")" && ScreenSize="${ScreenSizeFloat%.*}"
    [[ ! "${ScreenSize%.*}" -gt 0 || ! "$ScreenSize" =~ ^[[:digit:]]{1,2}$ ]] && until [[ "${ScreenSize%.*}" -gt 0 && "$ScreenSize" =~ ^[[:digit:]]{1,2}$ ]]; do clear; read -n "2" -p "Unable to determine screen size, please specify the screen size manually: " ScreenSize; done
    syncronize_variables ScreenSize ScreenSizeFloat GRVariablesUpload=""
    return
}
hdd_bay_count() {
    clear
    read -n "2" -p "Please specify the number of hard drive bays: " HDDBayCount
    [[ "$HDDBayCount" =~ ^[[:digit:]]?$ ]] || hdd_bay_count
    syncronize_variables HDDBayCount GRVariablesUpload=""
    return
}
webcam() {
    [[ -e "/dev/video0" || "$(grep -i -c -m "1" "camera" ${PCDDIR}testlogs/${AssetID:-"${SERIAL}"}-SYSINFO.yaml)" == "1" ]] && Webcam="WEBCAM INCLUDED" || Webcam="NO WEBCAM"
    syncronize_variables Webcam GRVariablesUpload=""
    return
}
skip_to_menu() {
    clear
    read -n "1" -t "30" -p "Would you like to bypass the HDD wipe and proceed to the main menu? [y/N]: "
    [[ "$REPLY" == "Y" ]] && SkipBootWipe="True" || SkipBootWipe=""
    return
}
bypass_diagnostic() {
    clear
    read -n "1" -t "60" -p "Would you like to bypass the diagnostic even if the system is above tech cut? [y/N]: "
    [[ "$REPLY" == Y ]] && BypassDiagnostic="True" || BypassDiagnostic=""
    return
}
eject_optical() {
    [[ -e "/dev/cdrom" ]] && eject -m
    return
}
wait_for_network() {
    declare -i Count="0"
    until [[ "$(ping -W 5s -c 1 google.com 2>> ${PCDDIR}Logging/tracelog)" || "${Count}" == "20" ]]; do
        [[ "${Count}" == "5" ]] && dhclient -x 2>> ${PCDDIR}Logging/tracelog && dhclient
        [[ "${Count}" == "10" ]] && systemctl restart wicd
        sleep "2"
        Count+="1"
    done
    [[ "${Count}" == "20" && ! "$(ping -W 5s -c 1 google.com 2>> ${PCDDIR}Logging/tracelog)" ]] && read -n "1" -p "Network connection lost, please check the cable and press any key to retry." && wait_for_network
    return "0"
}
connect_wireless_network() {
    killall wpa_supplicant >> "${PCDDIR}Logging/tracelog" 2>&1
    wpa_passphrase "$WIFISSID" "$WIFIPASS" > /etc/wpa_supplicant.conf 2>> "${PCDDIR}Logging/tracelog"
    wpa_supplicant -B -iwlan0 -c/etc/wpa_supplicant.conf -Dnl80211 >> "${PCDDIR}Logging/tracelog" 2>&1
    dhclient wlan0 >> "${PCDDIR}Logging/tracelog" 2>&1
    return
}
record_metric_user_touch() {
    [[ ! -d ${SHARE}Metrics/$(date +%F)/user_touch/${TechID%%@*} ]] && mkdir -p ${SHARE}Metrics/$(date +%F)/user_touch/${TechID%%@*}
    [[ ! -f ${SHARE}Metrics/$(date +%F)/user_touch/${TechID%%@*}/${UUID} ]] && touch ${SHARE}Metrics/$(date +%F)/user_touch/${TechID%%@*}/${UUID}
    return
}
initiate_workflow() {
    define_constants
    define_shell_behavior
    define_variables
    asset_lookup
    update_erase_drive_configuration
    skip_to_menu
    [[ "${SkipBootWipe}" != "True" ]] && passes
    [[ "${SkipBootWipe}" != "True" ]] && bypass_diagnostic
    [[ "${SkipBootWipe}" == "True" ]] && main_menu
    [[ "$Sysinfo" != "${PASS}" ]] && sysinfo
    eject_optical
    [[ "$BatteryTest" ]] || battery_test
    [[ "$Input" != "True" && "$GUI" != "False" ]] && launch_input_terminal
    [[ "$HDDCOUNT" == "0" && "$SERVER" != "True" ]] && test_complete
    [[ "$SERVER" != "True" ]] && smart_test
    [[ "$SmartTest" == "${FAIL}" ]] && test_complete
    wipe
    [[ "$Erasure" == "${FAIL}" || "$BypassDiagnostic" == "True" ]] && test_complete
    [[ "$TechCut" ]] || techcut
    [[ ( "$Erasure" == "${PASS}" && "$TechCut" != "False" ) || ( "$SERVER" == "True" ) ]] && diagnostics
    test_complete
    main_menu
}
[[ "$1" == input ]] && input "$2"
initiate_workflow
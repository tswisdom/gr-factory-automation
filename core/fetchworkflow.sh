#!/usr/bin/env bash

wait_for_network() {
    declare -i Count="0"
    until [[ "$(ip route | grep default)" || "${Count}" == "20" ]]; do
        [[ "${Count}" == "5" ]] && dhclient -x 2>> ${PCDDIR}Logging/tracelog && dhclient
        [[ "${Count}" == "10" ]] && systemctl restart wicd
        sleep "2"
        Count+="1"
    done
    [[ "${Count}" == "20" && ! "$(ip route | grep default)" ]] && read -n "1" -p "Network connection lost, please check the cable and press any key to retry." && wait_for_network
    return "0"
}
define_constants() {
    configure_script_tracing
    WORKFLOW=$(grep -o -E "WORKFLOW=[[:alnum:][:punct:]]*" /proc/cmdline)
    PS4='\D{%F %T}: $LINENO + '
    wait_for_network && IP="$(ip route | awk '/default/ { print $3 }')"
    SHARE="/mnt/reminst/"
    AZURE="http://gridfunc.blob.core.windows.net/input-prod?sv=2017-04-17&ss=bfqt&srt=sco&sp=rwdlacup&se=2018-12-31T22:16:59Z&st=2017-08-25T14:16:59Z&spr=https,http&sig=4FNakR8bhe8PAwxLPhyGjbvCOQ6OB2918v6KHohkT%2BA%3D"
    PCDDIR="/usr/local/pcdoctor/bin/"
    return
}
configure_script_tracing() {
    exec 19>> /usr/local/pcdoctor/bin/Logging/tracelog
    export BASH_XTRACEFD="19"
    set -a -x
    return
}
connect_wireless_network() {
    killall wpa_supplicant >> "${PCDDIR}Logging/tracelog" 2>&1
    wpa_passphrase "GlobalTest" "GlobalGuest2016!" > /etc/wpa_supplicant.conf 2>> "${PCDDIR}Logging/tracelog"
    wpa_supplicant -B -iwlan0 -c/etc/wpa_supplicant.conf -Dnl80211 >> "${PCDDIR}Logging/tracelog" 2>&1
    dhclient wlan0 >> "${PCDDIR}Logging/tracelog" 2>&1
    return
}
mount_file_share() {
    [[ ! -e "${SHARE}" ]] && mkdir "${SHARE}"
    wait_for_network && mount -t cifs //${IP}/reminst "${SHARE}" -o username=Administrator,password=Braker1 2>> "${PCDDIR}Logging/tracelog"
    return
}
import_resources() {
    cp -f -r ${SHARE}Test\ Scripts/* "${PCDDIR}scripts/custom"
    cp -f -r ${SHARE}Images/* "${PCDDIR}images"
    cp -f "${SHARE}${WORKFLOW##*=}" "${PCDDIR}"
    return
}
fetch_workflow() {
    echo "Establishing connection to deployment server."
    configure_script_tracing
    define_constants
    connect_wireless_network &
    mount_file_share
    import_resources
    cd "${PCDDIR}"
    chmod 0755 "${WORKFLOW##*/}"
    bash "${WORKFLOW##*/}"
    exit
}
fetch_workflow